# Project Title

Oasis website

## Getting Started

### Dependencies

* For project configuration, see package.json

### Installing
* Installing all dependencies
```
yarn install
```

### Executing program
* User yarn/npm to excute project files
* Run development server for testing
```
yarn start
```
* Generate production build
```
yarn build
```

### Build folder location
* All compiled source code located in /build