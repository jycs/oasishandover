import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import _ from "lodash";
import { FormattedMessage } from "react-intl";
import ShoppingCartItem from "../shoppCartItem/shoppingCartItem"
import * as actions from "../../actions";
import "./ShoppingCart.css";

class ShoppingCart extends Component {

  constructor() {
    super();
    this.state = {
      rating: 3.5,
      emptyStarColor: "#D3D3D3",
      starColor: "#606266",
      Qty: 0,
      show: true,
      liked: false,
      localCart: [],
      subtotal: 0
    };
  }

  componentDidMount() {
    console.log("data in this.props.cartContent")
    console.log(this.props.cartContent)
    // set timeout for fetch data too often.
    // this.props.fetchProducts();
    this._fetchCart()
  }

  _fetchCart() {
    this.setState({
      localCart: JSON.parse(localStorage.getItem("shoppingCartLocal")),
    }, () => {
      console.log(this.state.localCart);
      this.onLoadCaltotal(this.state.localCart)
    });
  }

  IncrementItem = () => {
    this.setState({ Qty: this.state.Qty + 1 });
    console.log("added")
  }
  DecreaseItem = () => {
    if (this.state.Qty === 0) {
      this.setState({ Qty: this.state.Qty - 0 });
    } else this.setState({ Qty: this.state.Qty - 1 });
  }

  setWhishList = () => {
    this.setState({ liked: !this.state.liked });
  }

  handleRemoveCartItem = (newCart, deletedItem) => {

    this.props.removedCart(newCart).then(() => {
      console.log("removed persist redux")
      console.log(this.props.cartContent)
      console.log(newCart)

      this.setState({
        localCart: newCart
      }, () => {
        localStorage.setItem("shoppingCartLocal", JSON.stringify(this.state.localCart));
        console.log(this.state.localCart)
        this.renderProducts()
        console.log("render product called")
      })

    })

  }

  renderProducts() {
    if (_.isEmpty(this.state.localCart, true)) {
      return (
        <div className="empty-cart d-flex flex-column align-items-center justify-content-center">
          <img src="https://www.chocogrid.com/img/images/cart-empty.jpg" />

          <Link to="/product_list"><button className="">Go Shopping</button></Link>
        </div>
      )
    }
    // /* map data returned, limited by 4  */
    return _.map(this.state.localCart, (value, i) => {
      return (
        <ShoppingCartItem key={i} handleRemove={this.handleRemoveCartItem} handlePriceChange={this.childCaltotal} id={value.id} idInCart={i} name={value.name} productPrice={value.price} itemColor={value.color} quantity={value.qty} productImg={value.productImg} parentCartData={this.state.localCart} lang={this.props.currentLocale.lang} />
      )
    });
  }

  onLoadCaltotal(cartData) {
    // console.log(this.state.localCart)
    let subtotal = _.map(cartData, (value, i) => {
      return (
        value.total
      )
    });
    const subtotalAmount = (subtotal.reduce(function (acc, val) {
      return acc + val
    }, 0))
    this.setState({
      subtotal: subtotalAmount
    }, () => {
      console.log(this.state.subtotal)
    })
  }

  childCaltotal = (amount) => {
    console.log("maoint?")
    if (amount) {
      this.setState({
        subtotal: amount
      });
    } else {
      // window.location.reload()
      this.setState({
        subtotal: amount
      });
    }

  }

  renderLabel() {
    switch (this.props.currentLocale.lang) {
      case "en":
        return " Enter coupon code here... ";
      case "sc":
        return " 请输入优惠码... ";
      case "tc":
        return " 請輸入優惠碼... ";
      default:
        return " Enter coupon code here... ";
    }
  }
  clearCart = (e) => {
    const temp = JSON.parse(localStorage.getItem("persist:cart"));
    localStorage.removeItem("shoppingCartLocal");
    this.props.clearCart();
    // localStorage.removeItem("persist:cart");
    this.setState({
      localCart: []
    }, () => {
      // this.renderProducts()
      this.props.history.push('/Cart')
      // localStorage.setItem("persist:cart", JSON.stringify(temp));
      window.location.reload();

    })
  }

  renderGrandtotal() {
    if (this.state.subtotal > 1500) {
      return this.state.subtotal
    } else if (this.state.subtotal == 0) {
      return this.state.subtotal + 0
    } else if (this.state.subtotal < 1500) {
      return this.state.subtotal + 70
    }
  }
  renderShipping() {
    if (this.state.subtotal > 1500) {
      return "0"
    } else if (this.state.subtotal == 0) {
      return "0"
    } else if (this.state.subtotal < 1500) {
      return "70"
    }
  }
  render() {
    const renderProducts = this.renderProducts();
    return (
      <div className="cart-container">
        <div className="page-path">
          <ul>
            <li><Link to="/">Home</Link></li>
            <li>/</li>
            <li><Link to="/product_list">Shop</Link></li>
            <li>/</li>
            <li><Link to="/cart">Cart</Link></li>
          </ul>
        </div>
        <div className="cart-table-container">
          <div className="page-title"><h6><FormattedMessage id="shoppingCartheader" /></h6></div>
          <div className="cart-table">
            <div className="cart-table-row-h">
              <div className="cart-table-cell-f">
                <h6></h6>
                <h6><FormattedMessage id="cartProducts" /></h6>
              </div>
              <div className="cart-table-cell"><h6><FormattedMessage id="unitPrice" /></h6></div>
              <div className="cart-table-cell"><h6><FormattedMessage id="qty" /></h6></div>
              <div className="cart-table-cell"><h6><FormattedMessage id="totalPrice" /></h6></div>
              <div className="cart-table-cell"></div>
            </div>
            {renderProducts}
            <div className="table-inteaction">
              <ul>
                <li>
                  <button onClick={this.clearCart}><FormattedMessage id="clearCart" /></button>
                </li>
                <li>
                  <Link to="/Cart"><button><FormattedMessage id="updateCartBtn" /></button></Link>
                </li>
                <li>
                  <a href={`/product_list?lang=${this.props.currentLocale.lang}`}><button><FormattedMessage id="continueShopping" /></button></a>
                </li>
              </ul>
            </div>

            <div className="table-interaction-mb">
              <div className="d-flex flex-column">
                <div className="d-flex flex-row">
                  <button className="mb-clear-cart mr-auto" onClick={this.clearCart}><FormattedMessage id="clearCart" /></button>
                  <Link className="mb-update-cart-btn" to="/Cart"><button><FormattedMessage id="updateCartBtn" /></button></Link>
                </div>

                <a className="mb-continue-shopping" href={`/product_list?lang=${this.props.currentLocale.lang}`}><button><FormattedMessage id="continueShopping" /></button></a>
              </div>
            </div>

            <div className="checkout-section">
              <div className="coupon-code">
                <ul>
                  <li><h5><FormattedMessage id="couponCode" /></h5></li>
                  <li><p><FormattedMessage id="couponDesc" /></p></li>
                  <li><input type="text" placeholder={this.renderLabel()} /></li>
                  <li><button><FormattedMessage id="applyCoupon" /></button></li>
                </ul>
              </div>
              <div className="cart-total">
                <ul>
                  <li><h5><FormattedMessage id="cartTotal" /></h5></li>
                  <li className="subtotal-amount">
                    <ul>
                      <li><p><FormattedMessage id="subTotal" /></p></li>
                      <li><p><FormattedMessage id="shipWaive" /></p></li>
                      <li><p><FormattedMessage id="grandTotal" /></p></li>
                    </ul>
                    <ul>
                      <li>$ {this.state.subtotal}</li>
                      <li>
                        +$
                      {this.renderShipping()}
                      </li>
                      <li>
                        <h5>
                          $
                          {this.renderGrandtotal()}
                        </h5></li>
                    </ul>
                  </li>
                  <li><button><FormattedMessage id="proceed" /></button></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    cartData: state.cartData,
    cartContent: state.cartContent,
    currentLocale: state.localization
  }
}

export default connect(mapStateToProps, actions)(ShoppingCart);