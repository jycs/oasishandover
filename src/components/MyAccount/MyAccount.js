import React, { Component } from 'react'
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { CountryDropdown } from 'react-country-region-selector';
import * as actions from "../../actions";
import requireAuth from "../HOC/requireAuth";
import "./MyAccount.css";

class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: '',
      region: '',
      isDiffShip: false
    };
  }
  selectCountry(val) {
    this.setState({ country: val });
  }

  selectRegion(val) {
    this.setState({ region: val });
  }
  onSubmit = formProps => {
    this.props.signin(formProps, () => {
      this.props.history.push("/feature");
    });
  };

  render() {
    const { handleSubmit } = this.props;// provide by redux form
    const { country, region } = this.state;
    return (
      <div className="custom-container">
        <div className="page-heading">
          <div className="page-path apply-margin-top-sm">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li>/</li>
              <li><Link to="/MyAccount">My Account</Link></li>
            </ul>
          </div>
        </div>
        <div className="d-flex flex-xl-row flex-lg-row flex-md-row flex-sm-column flex-column justify-content-center">
          <div className="mr-auto my-acc-vert-ctr d-flex flex-column col-xl-3 col-lg-3 col-md-3">
            <a href="#Dashboard">Dashboard</a>
            <hr />
            <a href="#Orders">Orders</a>
            <hr />
            <a href="#Downloads">Downloads</a>
            <hr />
            <a href="#Addresses">Addresses</a>
            <hr />
            <a class="active-selection" href="#Account Detail">Account Details</a>
            <hr />
            <Link to="signout">Log Out</Link>
            <hr />
          </div>
          <div className="billing-sec acc-detail-sec col-xl-8 col-lg-8 col-md-8 d-flex flex-column">
            <h4 className="checkout-form-header apply-margin-bt-sm">Billing Details</h4>
            <div className="billing-form account-detail-info-sec">
              <div className="billing-form-name d-flex flex-sm-column flex-md-row flex-lg-row flex-xl-row flex-column align-items-center justify-content-center">
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label  >First Name</label>
                  <Field name="fname" type="text" component="input" autoComplete="none" />
                </fieldset>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label  >Last Name</label>
                  <Field name="lname" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label  >Username</label>
                  <Field name="username" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label>Password</label>
                  <Field name="password" type="password" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label  >Email Address</label>
                  <Field name="email" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label  >Phone</label>
                  <Field name="phone" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked company-input d-flex flex-column">
                  <label  >Company</label>
                  <Field name="company" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked address-input d-flex flex-column">
                  <label  >Address</label>
                  <Field name="address-r1" type="text" component="input" autoComplete="none" />
                  <Field name="address-r2" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <div className="fieldset-stacked d-flex flex-column">
                  <label  >City</label>
                  <div>
                    <CountryDropdown
                      value={country}
                      onChange={(val) => this.selectCountry(val)} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    errorMessage: state.auth.errorMessage
  }
}

//compose allows to combine multiple hoc together, to apply in series
export default
  requireAuth(compose(
    connect(mapStateToProps, actions),
    reduxForm({ form: "signin" })
  )(MyAccount));
