import React, { Component } from 'react'
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { CountryDropdown } from 'react-country-region-selector';
import * as actions from "../../actions";
import PaypalBtn from "./PaypalBtn"
import axios from "axios";
import "./Checkout.css";
import qs from "qs";


const CLIENT = {
  sandbox: "AZ1aI5Fl4HiNMM1wUFU5uQnGp3wXWuK7iXe16X4uMqMHVtfbwGQoiLYaCrlkCGVF99rA8Rri65KPm9rS",
  production: "AZ1aI5Fl4HiNMM1wUFU5uQnGp3wXWuK7iXe16X4uMqMHVtfbwGQoiLYaCrlkCGVF99rA8Rri65KPm9rS"
};

const ENV = process.env.NODE_ENV === 'production'
  ? 'production'
  : 'sandbox';

class Checkout extends Component {
  constructor (props) {
    super(props);
    this.state = { 
      country: '', 
      dcountry:"",
      region: '',
      isDiffShip: false,
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL
    };
  }
  selectCountry (val) {
    this.setState({ country: val });
  }
  selectCountry2 (val) {
    this.setState({ dcountry: val });
  }
 
  selectRegion (val) {
    this.setState({ region: val });
  }
  onSubmit = formProps => {
  };

  //*****
  handleSubmit=(data)=> {
    const { 
      email, addressr1, addressr2, fname, lname, phone, company, areacode, country,
      demail, daddressr1, daddressr2, dfname, dlname, dphone, dcompany, dareacode, dcountry
    } = data
    const url = `${this.state.gUrl}/api/go_checkout.php`
    const payload ={
      lang:this.props.currentLocale.lang,
      payment:"paypal",
      token: "",
      Dcity:"va",
      Bciry:"sw",
      member:0,
      cartid: 2341,
      email:email,
      BFirstName:fname,
      BLastName:lname,
      Baddress1:addressr1,
      Baddress2:addressr2,
      Bcountry:this.state.country,
      Bcompany:company,
      phone:parseInt(phone),
      // Demail:demail,
      DFirstName: dfname,
      DLastName: dlname,
      Daddress1: daddressr1,
      Daddress2: daddressr2,
      Dcompany:dcompany,
      Dcountry:this.state.dcountry,
      areaCode:parseInt(areacode),

    }
    axios.post(url, qs.stringify(payload))
    .then(resp => {
    })
  }

  //error handling form validation
 required = value => value ? undefined : 'Required';

 maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

 number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined;

 minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;

 email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  'Invalid email address' : undefined;

 tooOld = value =>
  value && value > 65 ? 'You might be too old for this' : undefined;

  twarn = value =>
  value && /.+@aol\.com/.test(value) ?
  'Really? You still use AOL for your email?' : undefined;


  //error handling
   renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      <div>
        <input {...input} placeholder={label} type={type}/>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )

  toggleDiffShipForm=()=>{
    this.setState({
      isDiffShip: !this.state.isDiffShip
    });
  }
  renderDiffShipForm(){
    if(this.state.isDiffShip){
      return(
        <div className="show-diff-address apply-margin-top-lg">
        <div className="billing-form-name d-flex flex-row align-items-center justify-content-center">
          <fieldset className="fieldset-stacked d-flex flex-column">
            <label className="required-field">First Name</label>
            <Field name="dfname" type="text" component="input" autoComplete="none" />
          </fieldset>
          <fieldset className="fieldset-stacked d-flex flex-column">
            <label className="required-field">Last Name</label>
            <Field name="dlname" type="text" component="input" autoComplete="none" />
          </fieldset>
        </div>
        <div>
        <fieldset className="fieldset-stacked d-flex flex-column">
          <label className="required-field">Email Address</label>
          <Field name="demail" type="email"
             component={this.renderField} label="Email"
            validate={[this.required, this.email]}
            warn={this.twarn}
          />
        </fieldset>
      </div>

      <div className="billing-form-name d-flex flex-row align-items-center justify-content-center">
        <fieldset className="fieldset-stacked areacode d-flex flex-column">
          <label className="required-field">Area Code</label>
          <Field name="dareacode" type="text" component="input" autoComplete="none" />
        </fieldset>
        <fieldset className="fieldset-stacked d-flex flex-column">
        <label className="required-field">Phone</label>
        <Field name="dphone" type="text" component="input" autoComplete="none" />
        </fieldset>
      </div>

        <div>
        <fieldset className="fieldset-stacked company-input d-flex flex-column">
          <label className="">Company</label>
          <Field name="dcompany" placeholder="Optional" type="text" component="input" autoComplete="none" />
        </fieldset>
      </div>
        <div>
        <fieldset className="fieldset-stacked address-input d-flex flex-column">
          <label className="required-field">Address</label>
          <Field name="daddressr1" type="text" component="input" autoComplete="none" />
          <Field name="daddressr2" type="text" component="input" autoComplete="none" />
        </fieldset>
      </div>
      <div>
    </div>

        <div>
        <div className="fieldset-stacked d-flex flex-column">
          <label className="required-field">Country</label>
          <div>
            <CountryDropdown
              value={this.state.dcountry}
              onChange={(val) => this.selectCountry2(val)} />
          </div>
        </div>
        <fieldset className="fieldset-stacked address-input d-flex flex-column">
          <label className="">Order Notes</label>
          <Field name="notes" type="text" component="textarea" autoComplete="none" />
      </fieldset>
      </div>
      </div>
      );
    } 
  }

  render() {
    const maxLength15 = this.maxLength(15);
    const minValue18 = this.minValue(18);
    const { handleSubmit } = this.props;// provide by redux form
    const { country, region } = this.state;
    const renderDiffShipForm = this.renderDiffShipForm()

    const onSuccess = (payment) =>
    console.log('Successful payment!', payment);

    const onError = (error) =>
      console.log('Erroneous payment OR failed to load script!', error);

     const onCancel = (data) => console.log('Cancelled payment!', data);

    return (
      <div className="custom-container">
        <div className="page-heading">
          <div className="page-path apply-margin-top-sm">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li>/</li>
              <li><Link to="/product_list">Shop</Link></li>
              <li>/</li>
              <li><Link to="/checkout">Checkout</Link></li>
            </ul>
          </div>
          <div className="page-title apply-margin-top-md"><h6>Checkout</h6></div>
        </div>
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex flex-column justify-content-center">
          <div className="login-coupon-ads">
            <Link className="d-flex flex-row justify-content-start" to="/signin">Returning Customer?&nbsp;&nbsp;Click Here to Login.</Link>
          </div>
          <div className="login-coupon-ads">
            <Link className="d-flex flex-row justify-content-start" to="/signin">Have A Coupon?&nbsp;&nbsp;Click Here to Enter Your Code.</Link>
          </div>

          <form onSubmit={handleSubmit(this.handleSubmit)}>
          <div className="billing-sec 
          d-flex flex-row flex-xl-row 
          flex-lg-row flex-md-column 
          flex-sm-column flex-column">
            <div className="billing-form col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <h4 className="checkout-form-header">Billing Details</h4>
              <hr/>
              <div className="billing-form-name d-flex flex-row align-items-center justify-content-center">
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">First Name</label>
                  <Field name="fname" type="text" component="input" autoComplete="none" />
                </fieldset>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">Last Name</label>
                  <Field name="lname" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">Email Address</label>
                  <Field name="email" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>


              <div className="billing-form-name d-flex flex-row align-items-center justify-content-center">
              <fieldset className="fieldset-stacked areacode d-flex flex-column">
                <label className="required-field">Area Code</label>
                <Field name="areacode" type="text" component="input" autoComplete="none" />
              </fieldset>
              <fieldset className="fieldset-stacked d-flex flex-column">
              <label className="required-field">Phone</label>
              <Field name="phone" type="text" component="input" autoComplete="none" />
              </fieldset>
            </div>



              <div>
                <fieldset className="fieldset-stacked company-input d-flex flex-column">
                  <label className="">Company</label>
                  <Field name="company"  placeholder="Optional" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <fieldset className="fieldset-stacked address-input d-flex flex-column">
                  <label className="required-field">Address</label>
                  <Field name="addressr1" type="text" component="input" autoComplete="none" />
                  <Field name="addressr2" type="text" component="input" autoComplete="none" />
                </fieldset>
              </div>
              <div>
                <div className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">Country</label>
                  <div>
                    <CountryDropdown
                      value={this.state.country}
                      onChange={(val) => this.selectCountry(val)} />
                  </div>
                </div>
              </div>
    {/*
              <div className="d-flex flex-row align-items-center apply-margin-top-sm">
                <input type="checkbox" />
              <span className="remember-cred-label"> Create an account?</span>
            </div>
              <div className="login-reminder"> 
                  Create an account by entering the information below. 
                  if you are a returning customer please login at the top of the page.
            </div>
              <fieldset className="fieldset-stacked d-flex flex-column apply-margin-top-sm">
              <label className="required-field">Password</label>
              <Field name="password" type="password" component="input" autoComplete="none" />
             </fieldset>
             */}
              <div className="ship-to-different-address apply-margin-top-md d-flex flex-row align-items-center">
                <h6>Ship To A Different Address?</h6>
                <input type="checkbox" checked={this.state.isDiffShip} onChange={this.toggleDiffShipForm}/>
              </div>
              {renderDiffShipForm}
            </div>
            <div className="sub-total-sec col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <div className="sub-total-form d-flex flex-column justify-content-start align-items-start">
                <h4 className="checkout-form-header apply-margin-bt-md">Your Orders</h4>
                  <div className="checkout-form-col d-flex flex-row justify-content-center">
                    <div className="col-6 checkout-form-col-header">
                      <div className="">Product</div>
                    </div>
                    <div className="col-6 checkout-form-col-header">
                      <div className="">Total</div>
                    </div>
                  </div> 
                  <hr/>
                  <div className="checkout-form-col d-flex flex-row justify-content-center">
                    <div className="col-6">
                      <div className="">Das Bear x2</div>
                    </div>
                    <div className="col-6">
                      <div className="">$500.00</div>
                    </div>
                  </div> 
                  <hr/>
                  <div className="checkout-form-col d-flex flex-row justify-content-center">
                    <div className="col-6">
                      <div className="">Cras Neque Metus x2</div>
                    </div>
                    <div className="col-6">
                      <div className="">$1800.00</div>
                    </div>
                  </div> 
                  <hr/>
                  <div className="checkout-form-col d-flex flex-row justify-content-center">
                    <div className="col-6">
                      <div className="checkout-cart-subtotal">Cart Subtotal</div>
                    </div>
                    <div className="col-6">
                      <div className="checkout-cart-subtotal">$1800.00</div>
                    </div>
                  </div> 
                  <div className="checkout-form-col d-flex flex-row justify-content-center">
                    <div className="col-6">
                      <div className="checkout-cart-subtotal">Order Subtotal</div>
                    </div>
                    <div className="col-6">
                      <div className="checkout-cart-order-subtotal">$1800.00</div>
                    </div>
                  </div> 
                  <div className="checkout-form-col place-order d-flex flex-row justify-content-center">
                  <a href="https://www.paypal.com/hk/signin?locale.x=zh_HK">
                  
                  <PaypalBtn
                    client={CLIENT}
                    env={ENV}
                    commit={true}
                    currency={'USD'}
                    total={1000}
                    onSuccess={onSuccess}
                    onError={onError}
                    onCancel={onCancel}
                  />

                  <button type="submit" disabled={this.props.submitting} className="place-order-btn">Place Order</button>
                </a>
                </div>
              </div>
              
            </div>
          </div>
          </form>

         </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    errorMessage: state.auth.errorMessage,
    currentLocale: state.localization,
  }
}

//compose allows to combine multiple hoc together, to apply in series
export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(Checkout);
