import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import scriptLoader from 'react-async-script-loader';

class PaypalButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showButton: false,
    };

    window.React = React;
    window.ReactDOM = ReactDOM;
  }

  componentDidMount() {
    const {
      isScriptLoaded,
      isScriptLoadSucceed
    } = this.props;

    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ showButton: true });
    }
  }


  componentWillReceiveProps(nextProps) {
    const {
      isScriptLoaded,
      isScriptLoadSucceed,
    } = nextProps;

    const isLoadedButWasntLoadedBefore =
      !this.state.showButton &&
      !this.props.isScriptLoaded &&
      isScriptLoaded;

    if (isLoadedButWasntLoadedBefore) {
      if (isScriptLoadSucceed) {
        this.setState({ showButton: true });
      }
    }
  }



  render() {
    const paypal = window.PAYPAL
    const {
      total,
      currency,
      env,
      commit,
      client,
      onSuccess,
      onError,
      onCancel,
    } = this.props;

    const {
      showButton,
    } = this.state;

    const payment = () =>
      paypal.rest.payment.create(env, client, {

        transactions: [{
          amount: {
            total: '30.11',
            currency: 'USD',
            details: {
              subtotal: '30.00',
              tax: '0.07',
              shipping: '0.03',
              handling_fee: '1.00',
              shipping_discount: '-1.00',
              insurance: '0.01'
            }
          },
          description: 'The payment transaction description.',
          custom: '90048630024435',
          //invoice_number: '12345', Insert a unique invoice number
          payment_options: {
            allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
          },
          soft_descriptor: 'ECHI5786786',
          item_list: {
            items: [
              {
                name: 'hat',
                description: 'Brown hat.',
                quantity: '5',
                price: '3',
                tax: '0.01',
                sku: '1',
                currency: 'USD'
              },
              {
                name: 'handbag',
                description: 'Black handbag.',
                quantity: '1',
                price: '15',
                tax: '0.02',
                sku: 'product34',
                currency: 'USD'
              }],
            shipping_address: {
              recipient_name: 'Brian Robinson',
              line1: '4th Floor',
              line2: 'Unit #34',
              city: 'San Jose',
              country_code: 'US',
              postal_code: '95131',
              phone: '011862212345678',
              state: 'CA'
            }
          }
        }],


      });

    const onAuthorize = (data, actions) =>
      actions.payment.execute()
        .then(() => {
          const payment = {
            paid: true,
            cancelled: false,
            payerID: data.payerID,
            paymentID: data.paymentID,
            paymentToken: data.paymentToken,
            returnUrl: data.returnUrl,
          };

          onSuccess(payment);
        });

    return (

      <div>
        {showButton && <paypal.Button.react
          env={env}
          client={client}
          commit={commit}
          payment={payment}
          onAuthorize={onAuthorize}
          onCancel={onCancel}
          onError={onError}
        />}
      </div>
    );


  }
}

export default scriptLoader('https://www.paypalobjects.com/api/checkout.js')(PaypalButton);