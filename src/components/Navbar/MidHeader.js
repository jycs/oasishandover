import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom"
import { slide as Menu } from 'react-burger-menu'
import _ from "lodash/core";
import * as actions from "../../actions";
import { UncontrolledCollapse } from 'reactstrap';
import './NavigationBar.css';
// import { Button } from 'reactstrap';

class MidHeader extends Component {
  constructor() {
    super()
    this.state = {
      lang: "en",

    }
  }
  componentDidMount() {
    // this.props.fetchProductCatalog(this.state.lang)
    this.getCurrentLanguage()
  }

  getCurrentLanguage() {
    this.setState({
      lang: this.props.currentLocale.lang
    })
  }

  handleInputChange = (e) => {
    this.setState({
      searchVal: e.target.value
    })
  }
  handleSubmit = (e) => {
    let url = `/product_list?lang=${this.state.lang}&search=${this.state.searchVal}`
    // alert(url);
    this.props.history.push(url);
    window.location.reload();
    e.preventDefault();
  }

  renderPlaceHolder() {
    switch (this.state.lang) {
      case "en":
        return "Search entire store here ...";
      case "sc":
        return "寻找喜爱的产品 ...";
      case "tc":
        return "尋找喜愛的產品 ...";
      default:
        return "Search entire store here...";
    }
  }


  renderProductCat() {
    return _.map(this.props.fetchedCatalog, (cat, i) => {
      let url = `/product_list?lang=${this.state.lang}&category=${cat.name}&cattype=cat&d=${cat.text}`
      return (
        <div key={i}>
          <a href="#" cat-name={cat.name} cat-type="cat" displayed-name={cat.text} className="cat-dropdown d-flex flex-row justify-content-start align-items-center" id={`toggler${i}`}>
            <span cat-name={cat.name} displayed-name={cat.text}>{cat.text}</span>
            {/**  <img src="/catArrow.svg" className="cat-arrow" aria-hidden="true" />*/}
          </a>
          <UncontrolledCollapse toggler={`toggler${i}`}>
            <a href={url} key={i} className="sub-cat" cat-name={cat.name} cat-type="cat" displayed-name={cat.text}>
              <FormattedMessage id="all" />
              {cat.text}
            </a>
            {this.renderSubCat(i)}
          </UncontrolledCollapse>
        </div>
      )
    });
  }
  renderSubCat(i) {
    let subCatArray = _.map(this.props.fetchedCatalog[i].sub, (v, i) => {
      let url = `/product_list?lang=${this.state.lang}&category=${v.subname}&d=${v.subtext}`
      return (
        <div key={i} className="sub-cat">
          <a href={url} cat-name={v.subname} cat-type="sub" displayed-name={v.subtext} onClick={this.setSubCatFilter}>{v.subtext}</a>
        </div>
      )
    })
    return (subCatArray)

  }


  render() {
    return (
      <div className="mheader">
        <div className="mwrapper">
          <ul>
            <li className="lgbox"><Link to="/"><img alt="" className="Oasislogo" src="https://www.oasishomesupplies.com/files/logo/logo_tc.png" /></Link></li>
            <li>
              <form onSubmit={this.handleSubmit}>
                <div className="search-box-wrapper">
                  <input onChange={this.handleInputChange} type="text" placeholder={this.renderPlaceHolder()} className="search-box-input" />
                  <button type="submit" className="search-box-button"><i className="fa fa-search" /></button>
                </div>
              </form>
            </li>
            <li className="wishlist">
              <Link to="/wishlist">
                <div className="wishListImage">
                  <img alt="" src="/whish.svg" width="22" height="22" />
                  <div className="mbtn">
                    <FormattedMessage id="myWishlist" />
                    <div className={`lengthIndicator ${this.props.wishlistContent.wishlistArr.length <= 0 ? `indHidden` : `indShown`}`}>
                      {this.props.wishlistContent.wishlistArr.length ? this.props.wishlistContent.wishlistArr.length : 0}
                    </div>

                  </div>
                </div>
              </Link>
            </li>
            <li className="cart">
              <Link to="/Cart">
                <div className="cartImage">
                  <img alt="" src="/cart.svg" width="22" height="22" />
                  <div className="mbtn">
                    <FormattedMessage id="myCart" />
                    <div className={`lengthIndicator ${this.props.cartContent.cartArray.length <= 0 ? `indHidden` : `indShown`}`}>
                      {this.props.cartContent.cartArray.length ? this.props.cartContent.cartArray.length : 0}
                    </div>
                  </div>
                </div>
              </Link>
            </li>
          </ul>
        </div>

        {/* mobile */}
        <div className="mwrapper-mobile">
          <Menu customBurgerIcon={<img src="/menu.svg" />}>
            <li className="lgbox"><img alt="" className="Oasislogo" src="https://www.oasishomesupplies.com/files/logo/logo_tc.png" /></li>
            <li>
              <form onSubmit={this.handleSubmit}>
                <div className="search-box-wrapper">
                  <input onChange={this.handleInputChange} type="text" placeholder={this.renderPlaceHolder()} className="search-box-input" />
                  <button type="submit" className="search-box-button"><i className="fa fa-search" /></button>
                </div>
              </form>
            </li>
            <a href="/" className="cat-dropdown home-pd d-flex flex-row justify-content-start align-items-center" >
              <span>首頁</span>
            </a>
            <a href="/product_list" className="cat-dropdown all-pd d-flex flex-row justify-content-start align-items-center" >
              <span>所有產品</span>
            </a>
            {/* 
            <a id="home" className="menu-item" href="/">Home</a>
            <a id="about" className="menu-item" href="/about">About</a>
            <a id="contact" className="menu-item" href="/contact">Contact</a>
            <a onClick={this.showSettings} className="menu-item--small" href="">Settings</a>
            */}
            <div className="product-nav">
              {this.renderProductCat()}
            </div>
            <div className="mb-util">

            </div>
          </Menu>


          <Link to="/" className="mr-auto ml-auto">
            <img alt="" className="Oasislogo" src="https://www.oasishomesupplies.com/files/logo/logo_tc.png" />
          </Link>

          <Link className="wishBtnLink" to="/wishlist">
            <img className="wishBtn" alt="" src="/whish.svg" />
            <div className={`lengthIndicator ${this.props.wishlistContent.wishlistArr.length <= 0 ? `indHidden` : `indShown`}`}>
              {this.props.wishlistContent.wishlistArr.length ? this.props.wishlistContent.wishlistArr.length : 0}
            </div>

          </Link>

          <Link className="cartBtnLink" to="/Cart">
            <img className="cartBtn" alt="" src="/cart.svg" />

            <div className={`lengthIndicator ${this.props.cartContent.cartArray.length <= 0 ? `indHidden` : `indShown`}`}>
              {this.props.cartContent.cartArray.length ? this.props.cartContent.cartArray.length : 0}
            </div>

          </Link>

        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.auth.authenticated,
    currentLocale: state.localization,
    fetchedCatalog: state.fetchedCatalog,
    wishlistContent: state.wishlistContent,
    cartContent: state.cartContent,
  }
}
export default withRouter(connect(mapStateToProps, actions)(MidHeader));