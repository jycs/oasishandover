import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './NavigationBar.css';
import * as actions from "../../actions";
import { connect } from 'react-redux';
import { FormattedMessage } from "react-intl";
import _ from "lodash/core";

// import { Button } from 'reactstrap';

class BotHeader extends Component {
  constructor() {
    super()
    this.state = {
      lang: "",
      curUrl: "local",
      fetchedCatalog: [],
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL
    }
  }
  componentDidMount() {
    this.getCurrentLanguage()
  }

  getCurrentLanguage() {
    this.setState({
      lang: this.props.currentLocale.lang
    }, () => {
      this.props.fetchProductCatalog(this.state.lang)
    })
  }

  renderProductCat() {
    return _.map(this.props.fetchedCatalog, (cat, i) => {
      let url = `/product_list?lang=${this.state.lang}&category=${cat.name}&cattype=cat&d=${cat.text}`
      return (
        <li key={i}>
          <div className="bhdropdown">
            <a href={url} onClick={this.redirectmaincatPage} target-url={url}>
              <button className="dropbtn">
                {cat.text} <i className="fa fa-angle-down" />
              </button>
            </a>
            <div className="bhdropdown-content">
              <div className="bhdropdown-content-btn">
                {this.renderSubCat(i)}
              </div>
            </div>
          </div>
        </li>
      )
    });
  }

  redirectmaincatPage = (e) => {
  }
  redirectsubcatPage = (e) => {
    // console.log('')
    // let url = e.target.getAttribute('target-url')
    // this.props.history.replace(url);
  }
  renderSubCat(i) {
    let subCatArray = _.map(this.props.fetchedCatalog[i].sub, (v, i) => {
      let url = `${this.state.gUrl}/product_list?lang=${this.state.lang}&category=${v.subname}&d=${v.subtext}`
      return (
        <a href={url} onClick={this.redirectsubcatPage} key={i} cat-name={v.subname} displayed-name={v.subtext} className="bhdropdown-link" target-url={url}>
          {v.subtext}
        </a>
      )
    })
    return (subCatArray)

  }

  render() {
    return (
      <div className="bheader">
        <div className="bhwrapper">
          <ul>
            <li>
              <div className="bhdropdown">
                <Link to="/">
                  <button className="dropbtn">
                    <FormattedMessage id="home" /> {/* <i className="fa fa-angle-down" /> */}
                  </button>
                </Link>
              </div>
            </li>
            {this.renderProductCat()}
          </ul>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.auth.authenticated,
    currentLocale: state.localization,
    fetchedCatalog: state.fetchedCatalog
  }
}
export default connect(mapStateToProps, actions)(BotHeader);