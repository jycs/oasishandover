import React, { Component } from 'react'
import { connect } from 'react-redux'
import TopHeader from "./TopHeader"
import MidHeader from "./MidHeader"
import BotHeader from "./BotHeader"

export class NavigationBar extends Component {
  render() {
    return (
      <div>
        <TopHeader />
        <MidHeader />
        <BotHeader />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar)
