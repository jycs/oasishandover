import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from "react-intl";
import * as actions from "../../actions";
import './NavigationBar.css';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
// import * as actions from "../actions";

class TopHeader extends Component {
  constructor() {
    super()
    this.state = {
      isOpen: false,
      currentLanguageOption: "",
      currentCurrencyOption: "HKD",
      languageOption: [
        { locale: "en", lang: "en", title: "English" },
        { locale: "zh", lang: "tc", title: "中文(繁)" },
        { locale: "zh", lang: "sc", title: "中文(簡)" },
      ],
      currencyOption: ["HKD", "USD", "RMB"],
    };
  }

  _changeLanguage(e) {
    this.props.changeLanguage(e).then(() => {
      this.setState({
        currentLanguageOption: e.title
      })
    }).then(() => {
      this.props.history.push(`/?lang=${e.lang}`)
    })
    window.location.reload();
  }

  changeCurrency(e) {
    this.setState({
      currentCurrencyOption: e
    })
  }
  componentDidMount() {
    this.getCurrentLanguage()
  }
  getCurrentLanguage() {
    this.setState({
      currentLanguageOption: this.props.currentLocale.title
    })
  }
  render() {
    return (
      <div className="">
        <div className="d-flex flex-row justify-content-start align-items-center thstyle">
          <div>
            <UncontrolledDropdown>
              <DropdownToggle caret className="dd-toogle">
                {this.state.currentLanguageOption}
              </DropdownToggle>
              <DropdownMenu className="dd-menu">
                <DropdownItem onClick={this._changeLanguage.bind(this, this.state.languageOption[0])}>{this.state.languageOption[0].title}</DropdownItem>
                <DropdownItem onClick={this._changeLanguage.bind(this, this.state.languageOption[1])}>{this.state.languageOption[1].title}</DropdownItem>
                <DropdownItem onClick={this._changeLanguage.bind(this, this.state.languageOption[2])}>{this.state.languageOption[2].title}</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
          {/*
          <div className="v-devide"></div>
          <div className="mr-auto">

            <UncontrolledDropdown>
              <DropdownToggle caret className="dd-toogle">
                {this.state.currentCurrencyOption}
              </DropdownToggle>
              <DropdownMenu className="dd-menu">
                <DropdownItem onClick={this.changeCurrency.bind(this, this.state.currencyOption[0])}>{this.state.currencyOption[0]}</DropdownItem>
                <DropdownItem onClick={this.changeCurrency.bind(this, this.state.currencyOption[1])}>{this.state.currencyOption[1]}</DropdownItem>
                <DropdownItem onClick={this.changeCurrency.bind(this, this.state.currencyOption[2])}>{this.state.currencyOption[2]}</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
          */}
          <div className="ml-auto">
            {
              this.props.authenticated ?
                <UncontrolledDropdown>
                  <DropdownToggle caret className="dd-toogle">
                    <FormattedMessage id="myAccount" />
                  </DropdownToggle>
                  <DropdownMenu className="dd-menu">
                    <DropdownItem ><Link to="/MyAccount"><FormattedMessage id="accountSettings" /></Link></DropdownItem>
                    <DropdownItem ><FormattedMessage id="signOut" /></DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
                :
                <UncontrolledDropdown>
                  <Link to="/signin">
                    <div className="dd-toogle btn btn-secondary">
                      <FormattedMessage id="signIn" />
                    </div>
                  </Link>
                </UncontrolledDropdown>
            }


          </div>
          {
            this.props.authenticated ?
              ""
              :
              <div className="v-devide"></div>
          }
          {
            this.props.authenticated ?
              <div>
                <UncontrolledDropdown>
                  <Link to="/signout">
                    <div className="dd-toogle btn btn-secondary">
                      <FormattedMessage id="signOut" />
                    </div>
                  </Link>
                </UncontrolledDropdown>
              </div>
              :
              <div>
                <UncontrolledDropdown>
                  <Link to="/Signup">
                    <div className="dd-toogle btn btn-secondary">
                      <FormattedMessage id="createAccount" />
                    </div>
                  </Link>
                </UncontrolledDropdown>
              </div>
          }
        </div>
      </div>
    )
  }


}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.auth.authenticated,
    currentLocale: state.localization
  }
}
export default withRouter(connect(mapStateToProps, actions)(TopHeader));