import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from "axios";
import qs from "qs";
import _ from "lodash";
import { Link } from "react-router-dom";
import "./shoppingCartItem.css";
import * as actions from "../../actions";
// import lifecycle from 'react-pure-lifecycle';

class shoppingCartItem extends Component {

  constructor() {
    super();
    this.state = {
      emptyStarColor: "#D3D3D3",
      starColor: "#606266",
      Qty: 0,
      counter: 0,
      id: "",
      name: "",
      localizedName: "",
      img: "",
      price: "",
      colour: [],
      colorImgSeq: [],
      category: "",
      total: 0,
      cartItems: [],
      edMode: false,
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL
    };
  }

  componentDidMount() {
    this._syncCart()
  }


  static getDerivedStateFromProps(nextProps, prevState) {

    if (nextProps.id !== prevState.id) {
      return { id: nextProps.id };
    }
    else {
      return null;
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevProps.id !== this.state.id) {
      this._reFetchProductInfo(this.state.id, this.props.currentLocale.lang
      );
    }
  }

  _reFetchProductInfo(id) {
    const url = `${this.state.gUrl}/api/product_detail.php`
    const payload = {
      id: id,
      lang: this.props.currentLocale.lang
    }
    axios.post(url, qs.stringify(payload)) // need error handling later
      .then(resp => {
        this.setState({
          localizedName: resp.data.name,
          stock: resp.data.stock,
          price: resp.data.price,
          id: this.props.id
        })
      })
  }


  _syncCart() {
    this.setState({
      cartItems: JSON.parse(localStorage.getItem("shoppingCartLocal")),
      counter: this.props.quantity
    }, () => {
      const url = `${this.state.gUrl}/api/product_detail.php`
      const payload = {
        id: this.props.id,
        lang: this.props.lang
      }
      axios.post(url, qs.stringify(payload))
        .then(resp => {
          this.setState({
            localizedName: resp.data.name,
          })
        })

    })
  }

  IncrementItemInCart = (e) => {
    this.setState({ Qty: this.state.Qty, counter: this.state.counter + 1 }, () => {
      this._updateCart(1)
    })
  }

  DecreaseItemInCart = (e) => {
    if (this.state.counter === 1) {
      this.setState({ Qty: this.state.Qty - 0, counter: this.state.counter - 0 }, () => {
        this._updateCart(0)
      });
    } else if (this.state.counter > 1) {
      this.setState({ Qty: this.state.Qty, counter: this.state.counter - 1 }, () => {
        this._updateCart(-1)
      });
    }

  }
  removeItemInCart = (e) => {
    // let cartItems = this.state.cartItems
    let cartItems = this.props.parentCartData

    let deletedItem = this.state.cartItems[this.props.idInCart]
    cartItems.splice(this.props.idInCart, 1)

    this.props.handleRemove(cartItems, deletedItem)

    this.handleSubtotalCal(cartItems)
    this.setState({
      cartItems
    }, () => {
      this.handleSubtotalCal(this.props.parentCartData)
    })

  }


  _updateCart(qty) {
    // 1. get cart data in state
    let cartItems = this.props.parentCartData,
      id = this.props.id,
      color = this.props.itemColor;


    // 2. find the index(location) of the specific product stored in the cart array
    let itemIndex = _.findIndex(cartItems, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })
    // 2.5 load origin originQty
    let originQty = cartItems[itemIndex].qty,
      uPrice = cartItems[itemIndex].price,
      newQty = originQty += qty;

    //3. replace content to the array with the latest qty
    let replacement = { ...cartItems[itemIndex], qty: newQty, total: (newQty * uPrice) }
    cartItems.splice(itemIndex, 1, replacement);

    //4. set state with the updated array
    this.setState({
      cartItems
    }, () => {
      localStorage.setItem("shoppingCartLocal", JSON.stringify(this.state.cartItems));
      // this.props.handlePriceChange(this.state.cartItems);
      this.handleSubtotalCal(this.state.cartItems)
    })
  }

  handleSubtotalCal(cartItems) {
    let subtotal = _.map(cartItems, (value, i) => {
      return (
        value.total
      )
    });
    const subtotalAmount = (subtotal.reduce(function (acc, val) {
      return acc + val
    }, 0))
    this.props.handlePriceChange(subtotalAmount)
  }

  render() {
    return (

      <div className="cart-table-row-col">
        <div className="cart-table-row">
          <div className="cart-table-cell-f">
            <div><Link to={`/product_detail/${this.props.id}`}><img src={this.props.productImg} alt={this.props.name} />
            </Link></div>
            <div className="dk-cart-pd-name"><Link to={`/product_detail/${this.props.id}`}>{this.state.localizedName}</Link></div>
          </div>
          <div className="cart-table-cell mb-cart-price">
            <span>HK ${this.props.productPrice * (this.state.Qty + this.props.quantity)}</span>
            <a className="mb-ed-cart" onClick={e => {
              this.setState({
                edMode: true
              })
            }}>Edit</a>
          </div>
          <div className="cart-table-cell dk-cart-item">

            <div className="cart-product-interaction dk-cart-item">
              <div className="cart-QTY">
                <div className="cart-QTY_btn">
                  <i onClick={this.IncrementItemInCart} className="fa fa-caret-up" aria-hidden="true" />
                  <p className="QTY_num">{this.state.Qty + this.props.quantity}</p>
                  <i onClick={this.DecreaseItemInCart} className="fa fa-caret-down" aria-hidden="true" />
                </div>
              </div>
            </div>

          </div>
          <div className="cart-table-cell dk-cart-item">HK ${this.props.productPrice * (this.state.Qty + this.props.quantity)}</div>
          <div className="cart-table-cell dk-cart-item"><i onClick={this.removeItemInCart} cus-data="custChilddata" className="fa fa-times" aria-hidden="true"></i></div>
        </div>


        <Link className="mb-cart-pd-name" to={`/product_detail/${this.props.id}`}>{this.state.localizedName} <span className="dp-qty"> x {this.props.quantity} </span></Link>

        <div className={
          this.state.edMode ?
            `mb-cart-interaction edModeOn `
            :
            `mb-cart-interaction edModeOff`
        }>

          <i onClick={this.removeItemInCart} cus-data="custChilddata" className="fa fa-times" aria-hidden="true"></i>
          <div className="mb-qty">Quantity</div>
          <div className="cart-QTY">
            <div className="cart-QTY_btn">
              <i onClick={this.IncrementItemInCart} className="fa fa-caret-up" aria-hidden="true" />
              <p className="QTY_num">{this.state.Qty + this.props.quantity}</p>
              <i onClick={this.DecreaseItemInCart} className="fa fa-caret-down" aria-hidden="true" />
            </div>
          </div>

          <a className="mb-update-cart ml-auto" onClick={e => {
            this.setState({
              edMode: false
            })
          }}>Update</a>

        </div>
        <hr className="row-divider" />
      </div>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    wishlistContent: state.wishlistContent,
    currentLocale: state.localization,
    cartContent: state.cartContent,
  }
}

export default connect(mapStateToProps, actions)(shoppingCartItem);