import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import _ from "lodash";
import axios from "axios";
import qs from "qs";
import { withRouter } from 'react-router-dom'
import { FormattedMessage } from "react-intl";
import StarRatingComponent from 'react-star-rating-component';
// import { UncontrolledCarousel } from 'reactstrap';
// import ProductCard from "../ProductList/ProductCard";
import Slider from "react-slick";
import * as actions from "../../actions";
import "./ProductDetail.css";

class ProductDetail extends Component {
  constructor() {
    super();
    this.state = {
      rating: 3.5,
      emptyStarColor: "#D3D3D3",
      starColor: "#606266",
      Qty: 1,
      TQty: 1,
      UQty: 0,
      id: "",
      show: true,
      liked: false,
      name: "",
      style: "",
      diaunit: "",
      img: "",
      rating1: 0,
      overview: "",
      width: "",
      price: "",
      height: "",
      depth: "",
      colour: [],
      defaultHeroImg: "",
      perspectiveViewImg: "",
      colorImgSeq: [],
      category: "",
      subcategory: "",
      colorSelected: "",
      localCart: [],
      localWishlist: [],
      inCart: false,
      inWishlist: false,
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL
    };
  }
  componentDidMount() {
    this._syncCart()
    this._syncWishlist()
    this.fetchProductInfo(this.props.match.params.id, this.props.currentLocale.lang);
    this.props.fetchProducts(this.props.currentLocale.lang);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.match.params.id !== prevState.id) {
      return { id: nextProps.match.params.id };
    }
    else {
      return null;
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // update state when props changes
    if (prevProps.cartContent.cartArray !== prevState.localCart) {
      this.setState({
        localCart: prevProps.cartContent.cartArray
      })
    }
    if (prevProps.match.params.id !== this.state.id) {
      this.fetchProductInfo(this.state.id, this.props.currentLocale.lang
      );
    }
  }

  fetchProductInfo(id, lang) {
    const url = `${this.state.gUrl}/api/product_detail.php`
    const payload = {
      id,
      lang
    }
    axios.post(url, qs.stringify(payload)) // need error handling later
      .then(resp => {
        const { name, style, diaunit, img, id, rating1, price, overview, height, width, depth, colour, category, subcategory, stock } = resp.data
        if (id == -1) {
          // this.props.history.push(`/product_list?lang=${ this.props.currentLocale.lang }`);
          window.location.href = `${this.state.gUrl}/product_list?lang=${this.props.currentLocale.lang}`
        }
        this.setState({
          name,
          style,
          id,
          diaunit,
          img,
          overview,
          rating1: parseInt(rating1),
          width,
          price,
          height,
          depth,
          category,
          subcategory,
          colour,
          stock,
          colorSelected: colour[0].colour ? colour[0].colour : "default",
          perspectiveViewImg: colour[0].img,
          defaultHeroImg: colour[0].img,
          inCart: false
        });
      }).then(() => {
        this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
        if (this.fetchCartContent(this.props.match.params.id, this.state.colorSelected)) {
          this.setState({
            inCart: true
          });
        }
      })
  }

  onColorChange = (e) => {
    e.preventDefault();
    this.slider.slickGoTo(0);
    this.setState({
      colorSelected: e.target.getAttribute('color-data'),
      defaultHeroImg: e.target.getAttribute('color-img'),
      perspectiveViewImg: e.target.getAttribute('color-img'),
      Qty: 1
    },
      () => {
        window.scrollTo(0, 0);
        this.fetchCartContent(this.props.match.params.id, this.state.colorSelected)
        this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
      }
    );
  }

  _syncCart() {
    this.setState({
      localCart: JSON.parse(localStorage.getItem("shoppingCartLocal")),
    })
  }
  _syncWishlist() {
    this.setState({
      localWishlist: JSON.parse(localStorage.getItem("wishlistLocal")),
    })
  }
  _addCart = (e) => {
    const itemDetails = {
      id: this.props.match.params.id,
      name: this.state.name,
      color: this.state.colorSelected,
      qty: this.state.Qty,
      price: this.state.price,
      total: this.state.Qty * this.state.price,
      productImg: this.state.defaultHeroImg
    }
    this.props.addCart(itemDetails).then(() => {
      localStorage.setItem("shoppingCartLocal", JSON.stringify(this.props.cartContent.cartArray));
    }).then(() => {
      this.setState({
        inCart: true,
        localCart: this.props.cartContent.cartArray
      }
      )
    });


  }


  // detetch if in the cart
  fetchCartContent(id, color) {

    this.setState({
      localCart: JSON.parse(localStorage.getItem("shoppingCartLocal")),
    }, () => {
      let localCart = this.state.localCart
      //check in specific product is in cart
      if (_.find(localCart, function (element, index, collection) {
        return (element.id == id && element.color == color)
      })
      ) {
        this.setState({
          inCart: true
        })
      } else {
        this.setState({
          inCart: false
        })
      }
    })

  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  IncrementItem = () => {

    if (this.state.inCart) {
      this.setState({ UQty: this.state.UQty + 1 });
    } else {
      this.setState({ Qty: this.state.Qty + 1 })
    }

  }
  DecreaseItem = () => {
    if (this.state.inCart) {
      if (this.state.UQty === 0) {
        this.setState({ UQty: this.state.UQty - 0 });
      } else {
        this.setState({ UQty: this.state.UQty - 1 });
      }
    } else {
      if (this.state.Qty === 1) {
        this.setState({ Qty: this.state.Qty - 0 });
      } else this.setState({ Qty: this.state.Qty - 1 });
    }
  }

  setWishList = () => {
    this.props.addWishlist(this.props.match.params.id, this.state.colorSelected, this.state.defaultHeroImg, this.state.colorSelected).then(() => {
      this.setState({
        localWishlist: this.props.wishlistContent.wishlistArr
      }, () => {
        localStorage.setItem("wishlistLocal", JSON.stringify(this.props.wishlistContent.wishlistArr))
        this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
      })
    })
  }

  _removeWishList = (e) => {

    // 1. get cart data in state
    let localWishlist = this.state.localWishlist,
      id = this.props.match.params.id,
      color = this.state.colorSelected;

    // 2. find the index(location) of the specific product stored in the cart array
    let itemIndex = _.findIndex(localWishlist, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })

    //3. replace content to the array with the latest qty
    localWishlist.splice(itemIndex, 1);

    //4. set state with the updated array
    this.setState({
      localWishlist
    })

    //4. sync new array with the local storage
    this.props.removeWishlist(localWishlist).then(() => {
      localStorage.setItem("wishlistLocal", JSON.stringify(this.state.localWishlist));
      this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
    });
  }




  _updateCart = (qty) => {
    // 1. get cart data in state
    let localCart = this.state.localCart,
      id = this.props.match.params.id,
      color = this.state.colorSelected;

    // 2. find the index(location) of the specific product stored in the cart array
    let itemIndex = _.findIndex(localCart, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })

    // 2.5 load origin originQty
    let originQty = localCart[itemIndex].qty,
      // let originQty = this.state.TQty,
      newQty = originQty += this.state.UQty;

    //3. replace content to the array with the latest qty
    let replacement = { ...localCart[itemIndex], qty: newQty }
    localCart.splice(itemIndex, 1, replacement);

    //4. set state with the updated array
    this.setState({
      localCart: localCart
    })

    //4. sync new array with the local storage
    this.props.updateCart(localCart).then(() => {
      localStorage.setItem("shoppingCartLocal", JSON.stringify(this.state.localCart));
    });

  }





  fetchWishlistContent(id, color) {
    if (_.find(this.state.localWishlist, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })
    ) {
      this.setState({
        inWishlist: true
      })
    } else {
      this.setState({
        inWishlist: false
      })
    }
  }

  onPerImgChange = (e) => {
    this.setState({
      defaultHeroImg: e.target.getAttribute('src')
    })
  }
  renderColorSelections() {
    return _.map(this.state.colour, t => {
      const cbtnStyle = {
        background: `#${t.rgb}`,
        height: `45px`,
        width: `45px`
      };
      "fa fa-check-circle color-check-mark"
      const checkMark = <i color-data={t.colour} color-img={t.img} className={this.state.colorSelected == "White" ? "fa fa-check-circle color-check-mark check-mark-b" : "fa fa-check-circle color-check-mark"} aria-hidden="true" ></i>;
      return (
        <button key={t.colour} className={this.state.colorSelected == t.colour ? "color-btn color-btn-active" : "color-btn"} color-data={t.colour} color-img={t.img} style={cbtnStyle} onClick={this.onColorChange}>
          {this.state.colorSelected == t.colour ? checkMark : ""}
        </button>
      );
    });
  }

  renderProducts() {
    if (_.isEmpty(this.props.products, true)) {
      return (
        <div >fetching data</div>
      )
    }
    /* map data returned, limited by 4  render suggested products */
    return _.map(_.take(this.props.products, 6), t => {
      return (
        <div key={t.id} className="r-pd-card">
          <Link to={`/product_detail/${t.id}`}>
            <img className="recommanded-pd" src={t.img} alt={t.name} />
          </Link>
        </div>
      )
    });
  }

  render() {
    const { name, style, diaunit, img, rating1, price, overview, height, width, depth, rating, emptyStarColor, starColor, stock, defaultHeroImg, perspectiveViewImg, category, subcategory, } = this.state;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      arrows: true,
      swipeToSlide: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    };
    const heroImageSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      arrows: false,
      initialSlide: 0,
      slickGoTo: true,
      swipeToSlide: true,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    const perspectiveImg = _.map(_.take(img, 10), img_src => {
      return (
        <li key={img_src}>
          <img src={img_src} alt="" onClick={this.onPerImgChange} />
        </li>
      );
    })
    const mbPerspectiveImg = _.map(_.take(img, 4), img_src => {
      return (
        <div key={img_src} className="mb-pd-img">
          <img src={img_src} alt="" onClick={this.onPerImgChange} />
        </div>
      );
    })
    const renderProducts = this.renderProducts();
    return (
      <div className="proudct-detail-container">
        <div className="page-path">
          <ul>
            <li><Link to="/"><FormattedMessage id="home" /></Link></li>
            <li>/</li>
            <li><Link to="/product_list"><FormattedMessage id="products" /></Link></li>
            <li>/</li>
            <li><Link to="/product_list">{name}</Link></li>
          </ul>
        </div>
        <Slider className="mb-slider" ref={slider => (this.slider = slider)} {...heroImageSettings}>
          <div className="mb-pd-img">
            <img src={this.state.defaultHeroImg} alt="" />
          </div>
          {mbPerspectiveImg}
        </Slider>
        <div className="row mb-row">
          <div className="product-detail d-flex flex-row flex-xl-row flex-lg-row flex-md-row flex-sm-column flex-column">
            <div className="product-detail-img">
              <img className="hero-pd-img" src={this.state.defaultHeroImg} alt="" />
              <ul className="sm-pd-img">
                {/** 
                <li>
                  <img src={perspectiveViewImg} alt="" onClick={this.onPerImgChange} />
                </li>
              */}
                {perspectiveImg}
              </ul>
            </div>
            <div className="product-detail-info">
              <ul>
                <li>
                  <h2>{name}</h2>
                </li>
                <li>
                  <StarRatingComponent
                    name="rate1"
                    starCount={5}
                    value={rating1}
                    starColor={starColor}
                    emptyStarColor={emptyStarColor}
                    editing={true}
                  />
                </li>
                <div className="stock-status">
                  <h5><FormattedMessage id="avalibility" /></h5>
                  <div className="stock-indicator">
                    {this.state.stock == 0 ? <i className="fa fa-times" aria-hidden="true" /> : <i className="fa fa-check" aria-hidden="true" />
                    }
                    <p>{this.state.stock == 0 ? <FormattedMessage id="outStock" /> : <FormattedMessage id="inStock" />}</p>
                  </div>
                </div>
                <li>
                  <h4>HKD$ {price} </h4>
                </li>
                <li>
                  <div className="color-pad">
                    <div className="color-label"><FormattedMessage id="colorOptions" /></div>
                    <div className="color-selections d-flex flex-row">
                      {this.renderColorSelections()}
                    </div>
                  </div>
                </li>
                <li>
                  <div className="product-interaction">
                    {this.state.inCart ?
                      <form className="QTY">
                        <div className="QTY_btn">
                          <i onClick={this.IncrementItem} className="fa fa-plus" aria-hidden="true" />
                          <p className="QTY_num">{this.state.UQty}</p>
                          <i onClick={this.DecreaseItem} className="fa fa-minus" aria-hidden="true" />
                        </div>
                      </form>
                      :
                      <form className="QTY">
                        <div className="QTY_btn">
                          <i onClick={this.IncrementItem} className="fa fa-plus" aria-hidden="true" />
                          <p className="QTY_num">{this.state.Qty}</p>
                          <i onClick={this.DecreaseItem} className="fa fa-minus" aria-hidden="true" />
                        </div>
                      </form>
                    }

                    {this.state.inCart ? <button type="button" className="add-to-cart" onClick={this._updateCart}><FormattedMessage id="updateCart" /></button> : <button className="add-to-cart" onClick={this._addCart}><FormattedMessage id="addCart" /></button>}
                    {this.state.inWishlist ?
                      <button className="whish-list-btn" onClick={this._removeWishList}>
                        <i className="fa fa-heart" aria-hidden="true" />
                      </button>
                      :
                      <button className="whish-list-btn" onClick={this.setWishList}>
                        <i className="fa fa-heart-o" aria-hidden="true" />
                      </button>
                    }

                  </div>
                </li>
                <li>
                  <h5><FormattedMessage id="overview" /></h5>
                  <p>
                    {overview}
                  </p>
                </li>
                <hr />
                <h5><FormattedMessage id="specification" /></h5>
                <li className="spec">
                  <ul>
                    <li>
                      <b><FormattedMessage id="width" /></b>
                    </li>
                    <li>
                      <b> <FormattedMessage id="depth" /></b>
                    </li>
                    <li>
                      <b><FormattedMessage id="height" /></b>
                    </li>
                    <li>
                      <b><FormattedMessage id="cate" /></b>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      {width} {diaunit}
                    </li>
                    <li>
                      {depth} {diaunit}
                    </li>
                    <li>
                      {height} {diaunit}
                    </li>
                    <li>
                      {category}
                    </li>
                  </ul>
                </li>
                <hr />
                <h5><FormattedMessage id="shippingInfo" /></h5>
                <li className="spec">
                  <ul>
                    <li>
                      <b><FormattedMessage id="domesShip" /></b>
                    </li>
                    <li>
                      <b><FormattedMessage id="IntShip" /></b>
                    </li>
                    <li>
                      <b><FormattedMessage id="shipWaive" /></b>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      HKD$70
                    </li>
                    <li>
                      HK$ 300
                    </li>
                    <li>
                      <FormattedMessage id="shipWaiveInfo" />
                    </li>

                  </ul>
                </li>
                <hr />
                <h5><FormattedMessage id="share" /></h5>
                <li className="share-social-link">
                  <ul>
                    <li>
                      <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/">
                        <img src="https://image.ibb.co/nNzuOe/twitter.png" alt="" />
                      </a>
                    </li>
                    <li>
                      <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/">
                        <img src="https://image.ibb.co/cZ18ie/tumblr.png" alt="" />
                      </a>
                    </li>
                    <li>
                      <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/">
                        <img src="https://image.ibb.co/fWizpK/ig_copy.png" alt="" />
                      </a>
                    </li>
                    <li>
                      <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/">
                        <img src="https://image.ibb.co/cdToie/ig.png" alt="" />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="recommand-section">
          <div className="section-header">
            <h1><FormattedMessage id="alsoLike" /></h1>

            <div className="title-divider">
              <hr />
              <i className="fa fa-star" aria-hidden="true"></i>
              <hr />
            </div>
          </div>
          <div className="recommanded-sec">
            <Slider {...settings} >
              {renderProducts}
            </Slider>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    productDetail: state.fetchedProductDetail,
    products: state.fetchedProducts,
    cartContent: state.cartContent,
    currentLocale: state.localization,
    wishlistContent: state.wishlistContent
  }
}

export default withRouter(connect(mapStateToProps, actions)(ProductDetail));
