import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from "../../actions";
class Signout extends Component {
  componentDidMount() {
    this.props.signout();
    window.location.assign(`${process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL}/`)
  }
  render() {
    return (
      <div>

      </div>
    );
  }
}

export default connect(null, actions)(Signout);