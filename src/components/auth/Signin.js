import React, { Component } from 'react'
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";
import axios from "axios";
import qs from "qs";
import "./auth.css";

class Signin extends Component {
  constructor() {
    super();
    this.state = {
      errMsg: ""
    };
  }
  componentDidMount() {
    this._checkAuth();
  }
  onSubmit = async formProps => {
    const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)

    const response = await axios.post(`${gUrl}/api/login.php`, qs.stringify(formProps));
    if (response.data.output == "error") {
      this.setState({
        errMsg: "Incorrect email or password"
      })
    } else {
      this.props.signin(response.data.output, () => {
        this.props.history.push("/product_list");

      });
    }
  };


  _checkAuth() {
    if (this.props.auth) {
      window.location.assign(`${process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL}/`)
    }
  }
  //error handling form validation
  required = value => value ? undefined : 'Required';

  maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;

  number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined;

  minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;

  email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
      'Invalid email address' : undefined;

  tooOld = value =>
    value && value > 65 ? 'You might be too old for this' : undefined;

  password = value => {
    value && value.length < 6 ? 'password length must be more than 6' : undefined;
  }

  twarn = value =>
    value && /.+@aol\.com/.test(value) ?
      'Really? You still use AOL for your email?' : undefined;

  renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )


  render() {
    const { handleSubmit } = this.props;// provide by redux form
    const maxLength6 = this.maxLength(6);
    return (
      <div className="custom-container-md">
        <div>
          <div className="page-path">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li>/</li>
              <li><Link to="/product_list">Login</Link></li>
            </ul>
          </div>
          <div className="page-title apply-margin-tb"><h6>Login Or Create An Account</h6></div>
        </div>
        <div className="row login-form-table">
          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <div>
              <h5>Login</h5>
              <p>If you have an account with us, please log in.</p>
            </div>
            <div className="login-form">
              <div className="error-msg-decor">{this.state.errMsg}
              </div>
              <form onSubmit={handleSubmit(this.onSubmit)}>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">Email Address</label>
                  <Field name="email" type="email"
                    component={this.renderField}
                    validate={[this.required, this.email]}
                    warn={this.twarn}
                  />
                </fieldset>
                <fieldset className="fieldset-stacked d-flex flex-column">
                  <label className="required-field">Password</label>
                  <Field name="password" type="password"
                    component={this.renderField}
                    validate={[this.required]}
                    warn={this.twarn}
                  />
                </fieldset>
                <div className="fieldset-stacked-row align-items-center d-flex flex-xl-row flex-lg-row flex-sm-row flex-row justify-content-between">
                  <div className="required-field-decor">
                    Required Fields
                </div>
                  <div className="forgot-passowrd-decor">
                    <a href="#fg-tpassword">
                      Forgot Your Password?
                  </a>
                  </div>
                </div>
                <div className="d-flex flex-row align-items-center">

                  {/*
                    <input type="checkbox" />
                  <span className="remember-cred-label"> Remember Me</span>*/
                  }
                </div>
                <div className="apply-margin-bt-lg"></div>
                <button type="submit" disabled={this.props.submitting} className="place-order-btn">Sign in</button>
              </form></div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <div>
              <h5>New Customers</h5>
              <span>By Ceating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</span>
              <div className="create-acc-btn">
                <Link to="/Signup">
                  <button className="Signup-sugg">Create An Account</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth.authenticated,
    errorMessage: state.auth.errorMessage
  }
}

//compose allows to combine multiple hoc together, to apply in series
export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(Signin);
