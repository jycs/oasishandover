import React, { Component } from 'react'
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import axios from "axios";
import qs from "qs";
import SelectList from 'react-widgets/lib/SelectList'
import Moment from 'moment'
import momentLocalizer from 'react-widgets-moment';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import * as actions from "../../actions";
import 'react-day-picker/lib/style.css';
import 'react-widgets/dist/css/react-widgets.css'
import "./auth.css";

// Moment.locale('en')
momentLocalizer(Moment)

class Signup extends Component {

  constructor(props) {
    super(props);

    this.state = {
      gender: "Male",
      title: "Mr",
      passwordIsMasked: true,
    };
  }

  _title = e => {
    if (e.target.value == "female") {
      this.setState({
        gender: e.target.value,
        title: "Ms"
      })
    } else {
      this.setState({
        gender: e.target.value,
        title: "Mr"
      })
    }
  }
  onSubmit = async formProps => {
    const promotion = parseInt(formProps.promotion ? "1" : "0");
    const newsletter = parseInt(formProps.newsletter ? "1" : "0");
    const areacode = parseInt("852");
    const phone = parseInt(formProps.phone);

    const title = this.state.title
    const gender = this.state.gender;
    const payload = {
      ...formProps,
      birthday: Moment(formProps.dob).format('YYYY-MM-DD'),
      gender,
      title,
      newsletter,
      promotion,
      areacode,
      phone,
      lang: this.props.currentLocale.lang
    }

    const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)

    const response = await axios.post(`${gUrl}/api/register.php`, qs.stringify(payload));
  };

  //error handling form validation
  required = value => value ? undefined : 'Required';

  maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;

  number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined;

  minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;

  email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
      'Invalid email address' : undefined;

  tooOld = value =>
    value && value > 65 ? 'You might be too old for this' : undefined;

  password = value => {

    value && value.length < 6 ? 'password length must be more than 6' : undefined;
  }

  twarn = value =>
    value && /.+@aol\.com/.test(value) ?
      'Really? You still use AOL for your email?' : undefined;

  renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )
  renderSelectList = ({ input, data }) =>
    <SelectList {...input}
      onBlur={() => input.onBlur()}
      data={data} />;

  pwMask = e => {
    this.setState(prevState => ({
      passwordIsMasked: !prevState.passwordIsMasked,
    }));
  }

  renderDateTimePicker = ({ input: { onChange, value }, showTime }) =>
    <DateTimePicker
      onChange={onChange}
      format="YYYY-MM-DD"
      time={false}
      placeholder={String(value)}
      value={!value ? null : new Date(value)}
    />


  render() {
    const { handleSubmit } = this.props;// provide by redux form
    return (
      <div className="custom-container-md">
        <div>
          <div className="page-path">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li>/</li>
              <li><Link to="/product_list">Login</Link></li>
            </ul>
          </div>
          <div className="page-title apply-margin-tb"><h6>Create An Account</h6></div>
        </div>
        <div className="row sign-up-form">
          <form className="innerform" onSubmit={handleSubmit(this.onSubmit)}>
            <div className="fieldset-row login-in-instead d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <div className="d-flex flex-column align-content-center justify-content-center flex-lx-row flex-lg-row flex-md-column flex-sm-column appt">
                <div className="already-have-acc">Already have an account?</div>
                <Link to="/signin">Log in Instead!</Link>
              </div>

              <div className="signup-input"></div>
            </div>

            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Social title</label>
              <div className="signup-input d-flex flex-row singup-input-chbox">
                <div className="d-flex flex-row align-items-center">
                  <Field name="title" type="radio" component="input" onChange={this._title} value="Male" autoComplete="none" />
                  <span className="social-title-check">Mr.</span>
                </div>
                <div className="d-flex flex-row align-items-center">
                  <Field name="title" type="radio" component="input" onChange={this._title} value="Female" autoComplete="none" />
                  <span className="social-title-check">Ms.</span>
                </div>
              </div>
            </div>
            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>First Name</label>
              <div className="signup-input">
                <Field name="enfirstname" type="text"
                  component={this.renderField}
                  validate={[this.required]}
                  warn={this.twarn}
                />
              </div>
            </div>
            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Last Name</label>
              <div className="signup-input">
                <Field name="enlastname" type="text"
                  component={this.renderField}
                  validate={[this.required]}
                  warn={this.twarn}
                />
              </div>
            </div>
            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Email</label>
              <div className="signup-input">
                <Field name="email" type="email"
                  component={this.renderField}
                  validate={[this.required, this.email]}
                  warn={this.twarn}
                />
              </div>
            </div>
            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Phone</label>
              <div className="signup-input">
                <Field name="phone" type="number"
                  component={this.renderField}
                  validate={[this.required]}
                  warn={this.twarn}
                />
              </div>
            </div>

            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Password</label>
              <div className="signup-input password signup-input-pwi d-flex flex-row">
                <Field name="password" type={this.state.passwordIsMasked ? 'password' : 'text'}
                  Show={true}
                  component={this.renderField}
                  validate={[this.required]}
                  warn={this.twarn}
                />
                <a className="showpw" onClick={this.pwMask}>Show</a>
              </div>
            </div>

            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label>Birthdate</label>
              <div className="signup-input">

                <Field
                  name="birthday"
                  showTime={false}
                  component={this.renderDateTimePicker}
                />


                {/** 
                <DayPickerInput onDayChange={day => console.log(day)} />
                  */}

              </div>
            </div>

            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label></label>
              <div className="signup-input singup-input-chbox">
                <div className="d-flex flex-row align-items-center">
                  <Field name="promotion" type="checkbox" value="1"
                    component={this.renderField}
                    warn={this.twarn}
                  />
                  <span className="remember-cred-label">Receive offers from our partners</span>
                </div>
              </div>
            </div>
            <div className="fieldset-row d-flex flex-column align-content-center justify-content-start flex-lx-row flex-lg-row flex-md-column flex-sm-column">
              <label></label>
              <div className="signup-input singup-input-chbox">
                <div className="d-flex flex-row align-items-start">
                  <Field name="newsletter" type="checkbox" value="1"
                    component={this.renderField}
                    warn={this.twarn}
                  />
                  <span className="signup-news-letter"> Sign up for our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.</span>
                </div>
              </div>
            </div>
            <div>{this.props.errorMessage}
            </div>
            <div className="apply-margin-bt-lg"></div>


            <div className="signup-input singup-input-chbox">
              <div className="d-flex flex-row justify-content-center align-items-start">
                <button className="sif-submit-btn">Sign Up</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    errorMessage: state.auth.errorMessage,
    currentLocale: state.localization
  }
}

//compose allows to combine multiple hoc together, to apply in series
export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "signup" })
)(Signup);
