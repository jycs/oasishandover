import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

class ProductCard extends Component {
  constructor() {
    super();
    this.state = {
      isInWishlist: false,
      localWishlist:[]
    };
  }

  componentDidMount() {
    this._syncWishlist()
  }

  _syncWishlist() {
    if(this.props.wishListArr.find( list => list.id == this.props.id && list.color == this.props.color)){
      this.setState({
        isInWishlist: true
      })
    }
  }

  handleAddWishlist=(e)=>{
    this.props.addWishlist(
        e.target.dataset.id,
        e.target.dataset.color, 
        e.target.dataset.img
    )
    this.setState({
      isInWishlist: !this.state.isInWishlist
    })
  }

  handleRemoveWishlist=(e)=>{
    this.props.removeWishlist(
        e.target.dataset.id,
        e.target.dataset.color, 
        e.target.dataset.img
    )
    this.setState({
      isInWishlist: !this.state.isInWishlist
    })
  }

render(){
  return (
    <div>
      <div className="product-list-each">
    <div className="list-card-overlay 
    d-flex flex-row 
    justify-content-center align-items-center">
    {
      this.state.isInWishlist? 
      <img data-color={this.props.color} data-id={this.props.id} data-img={this.props.img} onClick={this.handleRemoveWishlist} src="/addedwish.png" className="pd-int-btn"></img>
      :
      <img data-color={this.props.color} data-id={this.props.id} data-img={this.props.img} onClick={this.handleAddWishlist} src="/addwish.png" className="pd-int-btn"></img>

    } 
      


      <Link to={`/product_detail/${this.props.id}`} className="pd-int-btn">
        <img src="/inspect.png"></img>
      </Link>
    </div>
      <Link to={`/product_detail/${this.props.id}`}>
    <img src={this.props.img} alt="{t.name}" />
      <h6>{this.props.name}</h6>
      <p>HK ${this.props.price}</p>
      </Link>
    </div>
    </div>
  );
}


};


const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    currentLocale: state.localization,
    wishlistContent: state.wishlistContent
  }
}

export default connect(mapStateToProps)(ProductCard);
