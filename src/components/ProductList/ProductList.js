import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { Range } from 'rc-slider';
import qs from "qs";
import ReactDOM from 'react-dom';
import { FormattedMessage } from "react-intl";
import axios from "axios"
import { UncontrolledCollapse } from 'reactstrap';
import queryString from 'query-string';
import _ from "lodash";
import * as actions from "../../actions";
import ProductCard from "./ProductCard"
import "./ProductList.css";
import 'rc-slider/assets/index.css';
import "rc-collapse/assets/index.css"
// import { StickyContainer, Sticky } from 'react-sticky';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ProductList extends Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this);
    this.state = {
      country: '',
      region: '',
      productList: [""],
      foundProductResult: 0,
      sliderValues: [0, 10000],
      lowerBound: 0,
      upperBound: 10000,
      lang: queryString.parse(this.props.location.search).lang ? queryString.parse(this.props.location.search).lang : "tc",
      catType: "",
      category: queryString.parse(this.props.location.search).category,
      pMin: 0,
      pMax: 99999,
      tag: undefined,
      order: "pA",
      cat: [],
      subCat: [],
      pdTag: [],
      payload: {},
      localWishlist: [],
      inWishlist: false,
      modal: false,
      static: false,
      titleDisplayed: queryString.parse(this.props.location.search).d,
      subtitleDisplayed: queryString.parse(this.props.location.search).d,
      searchVal: queryString.parse(this.props.location.search).search,
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL
    };

  }
  componentDidMount() {
    this.getCurrentLanguage()
    this.initProducts();
    this.fetchTags();
    this.fetchProductCat();
    this._syncWishlist()
    window.addEventListener('scroll', this.onListScroll, true);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.onListScroll, true);
  }

  _syncWishlist() {
    this.setState({
      localWishlist: JSON.parse(localStorage.getItem("wishlistLocal")),
    })
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }


  fetchProductCat() {
    const url = `${this.state.gUrl}/api/load_category.php?lang=${this.state.lang}`
    axios.get(url).then(resp => {
      this.setState({
        cat: resp.data,
        subCat: []
      }, () => {
      })
    })
  }

  getCurrentLanguage() {
    this.setState({
      lang: this.props.currentLocale.lang
    })
  }


  renderLabel() {
    switch (this.state.lang) {
      case "en":
        return "All";
      case "sc":
        return "所有";
      case "tc":
        return "所有";
      default:
        return "Search entire store here...";
    }
  }
  renderSortingLabel() {
    switch (this.state.lang) {
      case "en":
        return {
          defaultSorting: "Default Sorting",
          lpSorting: "Lowest Price",
          hpSorting: "Highest Price"
        };
      case "sc":
        return {
          defaultSorting: "预设排序",
          lpSorting: "最低价格",
          hpSorting: "最高价格"
        }
      case "tc":
        return {
          defaultSorting: "預設排序",
          lpSorting: "最低價格",
          hpSorting: "最高價格"
        }
      default:
        return "Search entire store here...";
    }
  }


  initProducts() {
    const url = this.state.searchVal ? `${this.state.gUrl}/api/product_search.php?lang=${this.state.lang}&name=${this.state.searchVal}` : `${this.state.gUrl}/api/product_output.php${this.props.location.search}`
    axios.get(url).then(resp => {
      this.setState({
        productList: resp.data,
        foundProductResult: resp.data.length,
      })
    });
  }
  fetchTags() {
    const url = `${this.state.gUrl}/api/load_tag.php?lang=${this.props.currentLocale.lang}`
    axios.get(url).then(resp => {
      this.setState({
        pdTag: resp.data
      })
    });
  }
  renderTags() {
    return _.map(this.state.pdTag, (val, i) => {
      return (
        <a key={i} href="#" onClick={this.settagFilter} tag-display={val.name} tag-name={val.id}>{val.name}</a>
      )
    })
  }
  renderProductCat() {

    return _.map(this.state.cat, (cat, i) => {
      return (
        <div key={i}>
          <a href="#" cat-name={cat.name} cat-type="cat" displayed-name={cat.text} className="cat-dropdown d-flex flex-row justify-content-start align-items-center" id={`toggler${i}`}>
            <img src="/catArrow.svg" className="cat-arrow" aria-hidden="true" />
            <span cat-name={cat.name} displayed-name={cat.text}>{cat.text}</span>
          </a>
          <UncontrolledCollapse toggler={`toggler${i}`}>
            <div key={i} className="sub-cat" cat-name={cat.name} cat-type="cat" displayed-name={cat.text} onClick={this.setCatFilter}>
              <FormattedMessage id="all" />
              {cat.text}
            </div>
            {this.renderSubCat(i)}
          </UncontrolledCollapse>
        </div>
      )
    });
  }
  settagFilter = (e) => {
    let empty;
    this.setState({
      tag: e.target.getAttribute('tag-name') ? e.target.getAttribute('tag-name') : empty,
      category: e.target.getAttribute('tag-name') ? e.target.getAttribute('tag-name') : empty,
      titleDisplayed: e.target.getAttribute('tag-display')
    }, () => {
      this.reFetchProductswithtag()
    })
  }
  setCatFilter = (e) => {
    let empty;
    this.setState({
      category: e.target.getAttribute('cat-name') ? e.target.getAttribute('cat-name') : empty,
      titleDisplayed: e.target.getAttribute('displayed-name'),
      catType: e.target.getAttribute('cat-type'),
      modal: false
    }, () => {
      this.reFetchProducts()
    })
  }
  setCatFilterAll = (e) => {
    let empty;
    this.setState({
      category: empty,
      titleDisplayed: e.target.getAttribute('displayed-name'),
      subtitleDisplayed: empty,
      tag: empty,
      modal: false
    }, () => {
      this.reFetchProducts()
      // this.toggle()
    })
  }
  reFetchProductswithtag = (e) => {
    let empty;
    let payload = {
      tag: this.state.tag ? this.state.tag : empty,
      pMin: this.state.lowerBound,
      pMax: this.state.upperBound,
      order: this.state.order,
      lang: this.state.lang,
    }
    const url = `${this.state.gUrl}/api/product_output.php`
    axios.post(url, qs.stringify(payload))
      .then(resp => {
        this.setState({
          productList: resp.data,
          foundProductResult: resp.data.length
        }, () => {
          this.renderProducts()
        })
      })
  }

  reFetchProducts = (e) => {
    let empty;
    let payload = {
      category: this.state.category,
      pMin: this.state.lowerBound,
      pMax: this.state.upperBound,
      order: this.state.order,
      lang: this.state.lang,
      cattype: this.state.catType
      // tag: this.state.tag
    }
    const url = `${this.state.gUrl}/api/product_output.php`
    axios.post(url, qs.stringify(payload))
      .then(resp => {
        this.setState({
          productList: resp.data,
          foundProductResult: resp.data.length,
          modal: false
        }, () => {
          this.renderProducts()
        })
      })
  }

  _addWishlist = (id, color, img) => {
    this.props.addWishlist(id, color, img).then(() => {
      this.setState({
        localWishlist: this.props.wishlistContent.wishlistArr
      }, () => {
        localStorage.setItem("wishlistLocal", JSON.stringify(this.props.wishlistContent.wishlistArr))
        this.fetchWishlistContent(id, img)
      })
    })
  }

  _removeWishList = (id, color, img) => {

    // 1. get cart data in state
    let localWishlist = this.state.localWishlist;

    // 2. find the index(location) of the specific product stored in the cart array
    let itemIndex = _.findIndex(localWishlist, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })

    //3. replace content to the array with the latest qty
    localWishlist.splice(itemIndex, 1);

    //4. set state with the updated array
    this.setState({
      localWishlist
    })

    //4. sync new array with the local storage
    this.props.removeWishlist(localWishlist).then(() => {
      localStorage.setItem("wishlistLocal", JSON.stringify(this.state.localWishlist));
      this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
    });
  }




  // _addWishlist = (e) => {
  //   this.props.addWishlist(e.target.getAttribute("pd-id"),
  //     e.target.getAttribute("df-color"), e.target.getAttribute("df-img"),
  //     this.state.colorSelected).then(() => {
  //       this.setState({
  //         localWishlist: this.props.wishlistContent.wishlistArr
  //       }, () => {
  //         localStorage.setItem("wishlistLocal", JSON.stringify(this.props.wishlistContent.wishlistArr))
  //         this.fetchWishlistContent(e.target.getAttribute("pd-id"), e.target.getAttribute("df-color"))
  //       })
  //     })
  // }

  fetchWishlistContent(id, color, wishlist) {
    if (_.find(wishlist, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })
    ) {
      return true
    } else {
      return false
    }

  }


  setSubCatFilter = (e) => {
    let empty;
    this.setState({
      category: e.target.getAttribute('cat-name') ? e.target.getAttribute('cat-name') : empty,
      titleDisplayed: e.target.getAttribute('displayed-name'),
      catType: e.target.getAttribute('cat-type'),
      modal: false
    }, () => {
      this.reFetchProducts()
    })
  }
  renderSubCat(i) {
    let subCatArray = _.map(this.state.cat[i].sub, (v, i) => {
      return (<div key={i} className="sub-cat" cat-name={v.subname} cat-type="sub" displayed-name={v.subtext} onClick={this.setSubCatFilter}>{v.subtext}</div>)
    })
    return (subCatArray)
  }
  renderRelatedProducts() {
    return _.map(_.take(this.state.productList, 2), (t, i) => {
      return (
        <div className="pdr-list-frame apply-margin-top-sm" key={i}>
          <Link to={`/product_detail/${t.id}`}>
            <div className="d-flex flex-row align-items-center">
              <div className="top-related-pd-img col-4">
                <img src={t.img} alt="{t.name}" /></div>
              <div className="top-related-pd-desc d-flex flex-column justify-content-start align-items-start col-8">
                <h6 className="top-related-pd-name">{t.name}</h6>
                <p className="top-related-pd-price">HK ${t.price}</p>
              </div>
            </div>
          </Link>
        </div>
      )
    });
  }


  renderProducts() {
    if (_.isEmpty(this.state.productList, true)) {
      return (
        <div className="not-found d-flex flex-column justify-content-center align-items-center">
          <div><FormattedMessage id="noProduct" /></div>
          <a onClick={this.setCatFilterAll} className="">
            <FormattedMessage id="seeOthers" />
          </a>
        </div>

      )
    }
    /* map data returned, limited by 4  */
    return _.map(this.state.productList, (t, i) => {
      return (

        <div className="pd-list-frame col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6" key={i}>
          <ProductCard id={t.id} img={t.img} addWishlist={this._addWishlist} removeWishlist={this._removeWishList} color={t.defaultcolour} price={t.price} fetchWish={this.fetchWishlistContent} name={t.name} wishListArr={this.props.wishlistContent.wishlistArr} />
        </div>

      )
    });
  }

  handleSortingChange = (e) => {
    window.scrollTo(0, 0);
    this.setState({
      order: e.target.value
    }, () => {
      this.state.tag ? this.reFetchProductswithtag() : this.reFetchProducts();
    })
  }
  handleSliderChange = sliderValues => {
    this.setState({
      lowerBound: sliderValues[0] * 50,
      upperBound: sliderValues[1] * 50,
    });
  }
  applyPriceFilterChange = e => {
    e.preventDefault();
    this.props.reFetchProducts();
  }

  toggle() {
    this.setState({
      modal: true
    });
  }
  toggleTrue = (e) => {
    this.setState({
      modal: true
    });
  }
  toggleFalse = (e) => {
    this.setState({
      modal: false
    });
  }

  onListScroll = (e) => {
    // if (e.target.scrollTop) {
    //   alert("hi")
    // }
    const targetHeight = document.querySelector('.pd-show-sec').offsetTop - 55.5 + 33
    if (window.scrollY > targetHeight) {
      this.setState({
        static: true
      })
    } else {
      this.setState({
        static: false
      })
    }
  }

  render() {
    const renderProducts = this.renderProducts();
    const renderRelatedProducts = this.renderRelatedProducts();
    return (
      <div className="custom-container-slg">
        <Modal isOpen={this.state.modal} backdrop={false} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggleFalse}>篩選</ModalHeader>
          <ModalBody>
            <div className="ranger-slider apply-margin-top-md">

              <Range min={0} max={200} defaultValue={this.state.sliderValues} onChange={this.handleSliderChange} />
            </div>
            <div className="range-display d-flex flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column justify-content-center align-content-center apply-margin-top-md">
              <div className="mr-auto ml-auto price-range-display-mb">
                <span><FormattedMessage id="price" />${this.state.lowerBound} - ${this.state.upperBound == 10000 ? this.renderLabel() : this.state.upperBound}</span>
              </div>
              <button className="range-filter-mb" onClick={this.reFetchProducts}><FormattedMessage id="filter" /></button>
            </div>

            <div className="mb-cat-sort d-flex flex-column justify-content-start">
              <div className="cata-header navs-mb">
                <h5><FormattedMessage id="categories" /></h5>
              </div>
              <div className="cata-navs navs-mb">
                <div>
                  <a onClick={this.setCatFilterAll} className="cat-dropdown d-flex flex-row justify-content-start align-items-center">
                    <img src="/catArrow.svg" className="cat-arrow" aria-hidden="true" />
                    <FormattedMessage id="all" />
                  </a>
                </div>
                {this.renderProductCat()}
              </div>
            </div>

          </ModalBody>
          {/**
          <ModalFooter>
            <Button color="primary" onClick={this.reFetchProducts}>套用</Button>
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
           */}

        </Modal>
        <div className="productListPage">
          <div className="page-path">
            <ul>
              <li><Link to="/"><FormattedMessage id="home" /></Link></li>
              <li>/</li>
              <li><Link to="/product_list"><FormattedMessage id="products" /></Link></li>
              <li>/</li>
              <li><Link to="/product_list">{this.state.titleDisplayed ? this.state.titleDisplayed : this.renderLabel()}</Link></li>
            </ul>
          </div>
        </div>
        <div className="d-flex flex-xl-row flex-lg-row flex-md-row flex-sm-row flex-column apply-margin-top-md">
          <div className="vertical-quick-nav position-fixed col-xl-3 col-lg-3 col-md-4" id="sticky-sidebar">
            <div className="vertical-pd-nav d-flex flex-column justify-content-start">
              <div className="cata-header">
                <h5><FormattedMessage id="categories" /></h5>
              </div>
              <div className="cata-navs">
                <div>
                  <a onClick={this.setCatFilterAll} className="cat-dropdown d-flex flex-row justify-content-start align-items-center">
                    <img src="/catArrow.svg" className="cat-arrow" aria-hidden="true" />
                    <FormattedMessage id="all" />
                  </a>
                </div>
                {this.renderProductCat()}
              </div>
            </div>
            <div className="cata-header apply-margin-top-md">
              <h5><FormattedMessage id="priceFilter" /></h5>
            </div>

            <div className="ranger-slider apply-margin-top-md">
              <Range min={0} max={200} defaultValue={this.state.sliderValues} onChange={this.handleSliderChange} />
            </div>
            <div className="range-display d-flex flex-xl-row flex-lg-row flex-md-column flex-sm-column flex-column justify-content-center align-content-center apply-margin-top-md">
              <div className="mr-auto price-range-display">
                <span><FormattedMessage id="price" />${this.state.lowerBound} - ${this.state.upperBound == 10000 ? this.renderLabel() : this.state.upperBound}</span>
              </div>
              <button className="range-filter" onClick={this.reFetchProducts}><FormattedMessage id="filter" /></button>
            </div>

            {/* related product*/}
            <div className="vertical-pd-nav d-flex flex-column justify-content-start apply-margin-top-lg">
              <div className="cata-header">
                <h5><FormattedMessage id="topRelated" /></h5>
              </div>
              {renderRelatedProducts}
            </div>

            {/* product tags */}
            <div className="vertical-pd-nav d-flex flex-column justify-content-start apply-margin-top-lg">
              <div className="cata-header">
                <h5><FormattedMessage id="productTags" /></h5>
              </div>
              <div className="product-tags d-flex flex-wrap justify-content-start align-content-center">
                <a href="#" onClick={this.setCatFilterAll}><FormattedMessage id="all" /></a>
                {this.renderTags()}
              </div>
            </div>
          </div>
          <div className="pd-show-sec ml-auto align-items-center col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
            <div className="page-title page-title d-flex flex-row justify-content-start align-items-center">
              <h6 >
                {this.state.titleDisplayed ? this.state.titleDisplayed : this.renderLabel()}
              </h6>
              <div className="mb-result"><FormattedMessage id="showing" />{this.state.foundProductResult}<FormattedMessage id="results" /></div>
            </div>
            <hr />

            <div className={
              `mb-filter
              d-flex flex-row justify-content-center align-items-center
              ${this.state.static ? `static` : `float`}
              `
            } ref="elem" onScroll={this.onListScroll}>
              <div className="mb-pd-filter" onClick={this.toggle}>
                <FormattedMessage id="filter" />
              </div>
              <div className="mb-sorting">
                <select onChange={this.handleSortingChange}>
                  <option value="pA">
                    {this.renderSortingLabel().defaultSorting}
                  </option>
                  <option value="pA">{this.renderSortingLabel().lpSorting}</option>
                  <option value="pD">{this.renderSortingLabel().hpSorting}</option>
                </select>
              </div>

            </div>

            {/* Quick sort */}
            <div className="hor-quick-sort d-flex col-12 flex-row justify-content-start align-items-center">

              <div className="sort-icon-sec col-2 d-flex flex-row justify-content-start align-items-center">
                <div className="sort-icon"><i className="fa fa-th" aria-hidden="true" /></div>
                <div className="sort-icon"><i className="sort-icon-row fa fa-bars" aria-hidden="true" /></div>
              </div>

              <div className="sorting-by col-10 d-flex flex-row justify-content-end align-items-center">
                <div><FormattedMessage id="sortBy" /></div>

                <div>
                  <select onChange={this.handleSortingChange}>
                    <option value="pA">
                      {this.renderSortingLabel().defaultSorting}
                    </option>
                    <option value="pA">{this.renderSortingLabel().lpSorting}</option>
                    <option value="pD">{this.renderSortingLabel().hpSorting}</option>
                  </select>
                </div>
                <div><FormattedMessage id="showing" />{this.state.foundProductResult}<FormattedMessage id="results" /></div>
              </div>
            </div>


            <hr />
            <div className="d-flex flex-wrap flex-row justify-content-center apply-margin-top-sm">
              {renderProducts}
            </div>
          </div>

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    currentLocale: state.localization,
    wishlistContent: state.wishlistContent
  }
}

export default connect(mapStateToProps, actions)(ProductList);