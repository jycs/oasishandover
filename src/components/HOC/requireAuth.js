import React, { Component } from 'react'
import { connect } from 'react-redux';

export default (ChildComponent) => {
  class ComposedComponent extends Component {
    componentDidMount() {
      this.shouldNavigateAway();
    }
    //our compoment just got updated
    componentDidUpdate() {
      this.shouldNavigateAway();
    }
    shouldNavigateAway() {
      if (!this.props.auth) {
        this.props.history.push("/signin");
      }
    }
    render() {
      return (
        <ChildComponent {...this.props} />
      );
    }
  }
  const mapStateToProps = (state, ownProps) => {
    return {
      auth: state.auth.authenticated
    }
  }

  return connect(mapStateToProps)(ComposedComponent);
};
