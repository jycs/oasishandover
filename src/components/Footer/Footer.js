import React from 'react';
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import './Footer.css';

const Footer = () => {
  const lang = JSON.parse(localStorage.getItem("localeLang")).lang
  return (
    <div className="footer-nav">
      <div className="container-fluid">
        <div className="row">
          <div className="col-xl-12">
            <div className="footer-links">
              <ul>
                <li><h4><FormattedMessage id="myAccount" /></h4></li>
                <li><Link to="/wishlist"><p><FormattedMessage id="myWishlist" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="creditSlips" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="order" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="personalInfo" /></p></Link></li>
              </ul>
              <ul>
                <li><h4><FormattedMessage id="shoppingGuide" /></h4></li>
                <li><Link to="/product_list"><p><FormattedMessage id="allCat" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="productReview" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="storeDirectory" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="aboutReturn" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="trackDelivery" /></p></Link></li>
              </ul>
              <ul>
                <li><h4><FormattedMessage id="aboutUs" /></h4></li>
                <li><Link to="/"><p><FormattedMessage id="aboutOasis" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="merchants" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="conditions" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="privacy" /></p></Link></li>
                <li><Link to="/"><p><FormattedMessage id="contactUs" /></p></Link></li>

              </ul>
              <ul>
                <li>
                  <div className="footer-company-info">
                    <i className="fa fa-map-marker" aria-hidden="true"></i>
                    <p>
                      <FormattedMessage id="address1" /><br />
                      {
                        lang == "en" ? <FormattedMessage id="address2" /> : ""
                      }
                      <br />
                      {
                        lang == "en" ? <FormattedMessage id="address3" /> : ""
                      }
                    </p>
                  </div>
                </li>
                <li>
                  <div className="footer-company-info">
                    <i className="fa fa-phone" aria-hidden="true"></i>
                    <a target="_blank" rel="noopener noreferrer" href="tel:+85227711660">
                      +852 2416 0155
                    </a>
                  </div>
                </li>
                <li>
                  <div className="footer-company-info">
                    <i className="fa fa-envelope" aria-hidden="true"></i>
                    <a href="mailto:info@oasishomesupplies.com" rel="noopener noreferrer" target="_blank" >
                      info@oasishomesupplies.com</a>
                  </div>
                </li>
                <li>
                  <div className="footer-company-info-social">
                    <div className="footer-company-social-icon">
                      <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/">
                        <i className="fa fa-facebook-square" aria-hidden="true" />
                      </a>
                    </div>
                    <div className="footer-company-social-icon">
                      <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/">
                        <i className="fa fa-instagram" aria-hidden="true" />
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="footer-download-tabs">
                    <a target="_blank" rel="noopener noreferrer" href="https://www.apple.com/ios/app-store/"><img src="http://solyogaflorida.com/wp-content/uploads/2018/03/download-on-app-store.png" alt="download app store" /></a>
                    <a target="_blank" rel="noopener noreferrer" href="https://play.google.com/store"><img src="https://watchelp-app.com/wp-content/uploads/2015/06/playstore.png" alt="Download on Play store" /></a>
                  </div>
                </li>
              </ul>
            </div>
            {/* Copy righ info*/}
            <div className="bottom-copyright">
              <ul>
                <li>
                  <p>Copyright Oasis Home Supplies 2018
                  All Rights Reserved</p>
                </li>
                <li>
                  <img src="https://imgur.com/j9du8U8.png" alt="payment method" />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default Footer