import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import _ from "lodash";
import { FormattedMessage } from "react-intl";
import * as actions from "../../actions";
import './Home.css';
import Slider from "react-slick";
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Button
} from 'reactstrap';

const items = [
  {
    src: 'https://i.imgur.com/Ow8J6tV.jpg',
    altText: 'WINTER SALES',
    caption: 'For only $40 you can own beautiful prints',
    key: 1
  },
  {
    src: 'https://cb2.scene7.com/is/image/CB2/072018_dining-chairs_home?wid=1680&qlt=60',
    altText: 'HOME SWEET HOME',
    caption: 'For only $40 you can own beautiful prints',
    key: 2
  },
  {
    src: 'http://belano.rs/wp-content/uploads/2018/09/dizajn-enterijera.jpg',
    altText: 'LATEST FURNITURE',
    caption: 'For only $40 you can own beautiful prints',
    key: 3
  }
];



class Home extends Component {

  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);

  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  //fetch data after component mounted(after rendered)
  componentDidMount() {
    // set timeout for fetch data too often.
    this.props.fetchProducts(this.props.currentLocale.lang);
  }
  renderTopProducts() {
    if (_.isEmpty(this.props.products, true)) {
      return <li >fetching data</li>
    }
    /* map data returned, limited by 4  */

    return _.map(_.take(this.props.products, 5), t => {
      return (
        <li key={t.id}>
          <Link to={`/product_detail/${t.id}`}>
            <img src={t.img} alt={t.name} />
          </Link>
        </li>
      )
    });
  }

  renderProducts() {
    if (_.isEmpty(this.props.products, true)) {
      return <li >fetching data</li>
    }
    /* map data returned, limited by 4  */
    return _.map(_.take(this.props.products, 4), t => {
      return (
        <li className="" key={t.id}>
          <Link to={`/product_detail/${t.id}`}>
            <div className="product-item">
              <img src={t.img} alt="{t.name}" />
              <h6>{t.name}</h6>
              <p>HK ${t.price}</p>
            </div>
          </Link>
        </li>
      )
    });
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      arrows: true,
      swipeToSlide: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1680,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            rows: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            rows: 2,
            slidesToScroll: 2
          }
        }
      ]
    };
    const settings2 = {

      dots: false,
      infinite: true,
      speed: 500,
      arrows: true,
      swipeToSlide: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1680,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,

            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,

            slidesToScroll: 2
          }
        }
      ]

    }
    const { activeIndex } = this.state;
    const renderProducts = this.renderProducts();
    const renderTopProducts = this.renderTopProducts();
    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.key}
        >
          <img className="carouselads" src={item.src} alt={item.altText} />
          <CarouselCaption captionText="" />
          <div className="text-block">
            <h1>{item.altText}</h1>
            <p className="cdesc">{item.caption}</p>
            <Link to="/product_list"><Button color="info">Shop Now</Button></Link>
          </div>
        </CarouselItem>
      );
    });

    return (
      <div>
        <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
          interval={6000}
        >
          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
          <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
        </Carousel>

        <div className="container-fluid">
          <div className="row">
            <div className="col-xl-12">
              {/* Featured products */}
              <div className="featured-products">
                {/* Title */}
                <div className="section-header-br">
                  <div className="section-header">
                    <h1><FormattedMessage id="featureProducts" /></h1>
                    <div className="title-divider">
                      <hr />
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <hr />
                    </div>
                  </div>
                </div>
                {/* Segmented control button */}
                <ul>
                  <li><Link to=""><FormattedMessage id="fp_dining" /></Link></li>
                  <li><Link to=""><FormattedMessage id="fp_office" /></Link></li>
                  <li><Link to=""><FormattedMessage id="fp_studyroom" /></Link></li>
                  <li><Link to=""><FormattedMessage id="fp_living" /></Link></li>
                </ul>
              </div>
              {/* Product List */}
              <Slider {...settings}>
                {renderProducts}
              </Slider>
              <div className="">
                {/**
                <ul className="row">
                {renderProducts}
                </ul>
               */}
              </div>
            </div>
          </div>
        </div>
        {/* Ads for mobile application */}
        <div className="mobile-app-adv d-flex flex-xl-row flex-lg-row flex-md-row flex-sm-column flex-column">

          <div className="download-tabs tab-mb">
            <Link to="/product_list"><Button color="info"><FormattedMessage id="aboutOasis" /></Button></Link>
            <a target="_blank" rel="noopener noreferrer" href="https://play.google.com/store"><img src="http://solyogaflorida.com/wp-content/uploads/2018/03/download-on-app-store.png" alt="download app store" /></a>
            <a target="_blank" rel="noopener noreferrer" href="https://www.apple.com/ios/app-store/"><img src="https://watchelp-app.com/wp-content/uploads/2015/06/playstore.png" alt="Download on Play store" /></a>
          </div>

          <div className="adv-images">
            <img src="https://image.ibb.co/m4k0L9/Artboard_Cop.png" alt="Oasis city app" />
          </div>
          <div className="adv-description d-flex flex-lx-row flex-lg-row flex-md-row flex-sm-column flex-column">
            <h1><FormattedMessage id="aboutSectiontitle" /></h1>
            <p><FormattedMessage id="aboutSectionDesc" />
            </p>

            <div className="download-tabs tab-dk">
              <Link to="/product_list"><Button color="info"><FormattedMessage id="aboutOasis" /></Button></Link>
              <a target="_blank" rel="noopener noreferrer" href="https://play.google.com/store"><img src="http://solyogaflorida.com/wp-content/uploads/2018/03/download-on-app-store.png" alt="download app store" /></a>
              <a target="_blank" rel="noopener noreferrer" href="https://www.apple.com/ios/app-store/"><img src="https://watchelp-app.com/wp-content/uploads/2015/06/playstore.png" alt="Download on Play store" /></a>
            </div>

          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xl-12">
              <div className="section-header">
                <h1><FormattedMessage id="topProducts" /></h1>
                <div className="title-divider">
                  <hr />
                  <i className="fa fa-star" aria-hidden="true"></i>
                  <hr />
                </div>
              </div>
              {/* Top product*/}
              <div className="top-product-list">

                <Slider {...settings2}>
                  {renderTopProducts}
                </Slider>
              </div>
              {/* OUR SELLERS*/}
              <div className="our-sellers">
                <div className="section-header">
                  <h1><FormattedMessage id="ourSellers" /></h1>
                  <div className="title-divider">
                    <hr />
                    <i className="fa fa-star" aria-hidden="true"></i>
                    <hr />
                  </div>
                </div>
                <div className="seller-list">
                  <ul>
                    <li>
                      <Link to="/productList"><img src="https://image.ibb.co/ekYLL9/brand_5.png" alt="" /></Link>
                    </li>
                    <li>
                      <Link to="/productList"><img src="https://image.ibb.co/cTcQnp/brand_4.png" alt="" /></Link>
                    </li>
                    <li>
                      <Link to="/productList"><img src="https://image.ibb.co/cTcQnp/brand_4.png" alt="" /></Link>
                    </li>
                    <li>
                      <Link to="/productList"><img src="https://image.ibb.co/hvNEf9/brand_3.png" alt="" /></Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  };
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    currentLocale: state.localization
  }
}

export default connect(mapStateToProps, actions)(Home);