import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import axios from "axios";
import _ from "lodash";
import qs from "qs";
import { FormattedMessage } from "react-intl";
import * as actions from "../../actions";
import "./Wishtlist.css";

class Wishlist extends Component {
  constructor() {
    super();
    this.state = {
      gUrl: process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL,
      img: "",
      name: "",
      stock: 0,
      price: 0,
      wishlistItems: [],
      idInWishlist: 0,
      pdid: 0,
      inCart: false
    };
  }

  componentDidMount() {
    // set timeout for fetch data too often.
    this._fetchProductInfo()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.pdid !== prevState.pdid) {
      console.log(nextProps.pdid)
      return { pdid: nextProps.pdid };
    }
    else {
      return null;
    }

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // update state when props changes
    if (prevProps.pdid !== this.state.pdid) {
      console.log(this.state.pdid)
      this._reFetchProductInfo(this.state.pdid, this.props.currentLocale.lang
      );
    }
  }

  _fetchCartContent(id, color) {

    this.setState({
      localCart: JSON.parse(localStorage.getItem("shoppingCartLocal")),
    }, () => {
      let localCart = this.state.localCart
      console.log("onColorChange");
      console.log(localCart);
      //check in specific product is in cart
      if (_.find(localCart, function (element, index, collection) {
        return (element.id == id && element.color == color)
      })
      ) {
        console.log("founded")
        this.setState({
          inCart: true
        }, () => {
          console.log("set true called")
        })
      } else {
        console.log("not founded")
        this.setState({
          inCart: false
        }, () => {
          console.log("set false called")
        })
      }
    })

  }

  _reFetchProductInfo(id) {
    const url = `${this.state.gUrl}/api/product_detail.php`
    const payload = {
      id: id,
      lang: this.props.currentLocale.lang
    }
    axios.post(url, qs.stringify(payload)) // need error handling later
      .then(resp => {
        this.setState({
          name: resp.data.name,
          stock: resp.data.stock,
          price: resp.data.price,
          idInWishlist: this.props.idInWishlist,
          pdid: this.props.pdid
        }, () => {
          console.log(resp.data)
        })
      })
  }


  _fetchProductInfo() {
    this.setState({
      wishlistItems: JSON.parse(localStorage.getItem("wishlistLocal")),
    }, () => {
      const url = `${this.state.gUrl}/api/product_detail.php`
      const payload = {
        id: this.state.wishlistItems[this.props.idInWishlist].id,
        lang: this.props.currentLocale.lang
      }
      axios.post(url, qs.stringify(payload)) // need error handling later
        .then(resp => {
          this.setState({
            name: resp.data.name,
            stock: resp.data.stock,
            price: resp.data.price,
            idInWishlist: this.props.idInWishlist,
            pdid: this.props.pdid
          }, () => {
            console.log(resp.data)
            this._fetchCartContent(this.props.pdid, this.props.pdcolor)
          })
        })
    })

  }
  // _addCart = (e) => {
  //   this._fetchProductInfo()

  //   let itemDetails = {
  //     id: this.props.pdid,
  //     name: this.state.name,
  //     color: this.props.pdcolor,
  //     qty: 1,
  //     price: this.state.price,
  //     total: this.state.price,
  //     productImg: this.props.img
  //   }
  //   console.log(itemDetails)
  //   this.props.addCart(itemDetails).then(() => {
  //     localStorage.setItem("shoppingCartLocal", JSON.stringify(this.props.cartContent.cartArray));
  //     this.removeItem()
  //   })

  // }


  _addCart = (e) => {
    this.props.handleAdd(
      this.props.pdid,
      this.state.name,
      this.props.pdcolor,
      this.props.img,
      1,
      this.state.price
    )
    this.removeItemInWishlist()
  }

  removeItemInWishlist = (e) => {
    console.log(this.props.pdid, this.props.pdcolor, this.props.idInWishlist)
    // this._fetchProductInfo()
    this.props.handleRemove(this.props.pdid, this.props.pdcolor, this.idInWishlist)
    // this.props.handleRemove(this.state.wishlistItems, deletedItem)
  }
  /** 
    removeItemInWishlist = (e) => {
      console.log('=====state , name==================');
      console.log(this.state.name);
      console.log('==========state.name==================');
  
      // let cartItems = this.state.cartItems
      let wishlistItems = this.props.wishlistData
      console.log("before splice id")
      console.log(this.props.idInWishlist)
      let deletedItem = this.state.wishlistItems[this.props.idInWishlist]
      wishlistItems.splice(this.props.idInWishlist, 1)
      console.log("aterSplace id")
      console.log(this.props.idInWishlist)
      console.log("wishlist Item")
      console.log(wishlistItems)
      this.setState({
        wishlistItems: wishlistItems
      }, () => {
        console.log(this.props.idInWishlist)
        this.props.handleRemove(this.state.wishlistItems, deletedItem)
        const url = `${this.state.gUrl}/api/product_detail.php`
        const payload = {
          id: this.props.pdid,
          lang: this.props.currentLocale.lang
        }
        // this.renderProductRow()
        console.log(payload)
        axios.post(url, qs.stringify(payload)) // need error handling later
          .then(resp => {
            console.log(resp)
            this.setState({
              name: resp.data.name,
              stock: resp.data.stock,
              price: resp.data.price
            })
          }, () => {
            console.log("wishListitems in child after setstate")
            console.log(this.state.wishlistItems)
            console.log('=====state , name==================');
            console.log(this.state.name);
            console.log('==========state.name==================');
            // window.location.reload();
  
          })
      })
    }
    */

  renderProductRow() {
    console.log(this.props.pdid)
    return (
      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div className="wishlist-table-row">
          <div className="mb-rm-btn wishlist-table-cell col-xl-1 col-lg-1 col-md-1 col-sm-1">
            <i onClick={this.removeItemInWishlist} className="fa fa-times" aria-hidden="true" />
          </div>
          <div className="wishlist-table-cell pd-img wishlist-table-cell-f col-xl-5 col-lg-5 col-md-5 col-sm-6">
            <div><Link to={`/product_detail/${this.props.pdid}`}><img src={this.props.img} alt={this.state.name} /></Link></div>

            <div className="dk-pd-name">
              <Link to={`/product_detail/${this.props.pdid}`}>{this.state.name}</Link>
            </div>

          </div>
          <div className="wishlist-table-cell dt-pd-price col-xl-2 col-lg-2 col-md-2 col-sm-2">HK ${this.state.price}</div>
          <div className="wishlist-table-cell dt-pd-stock col-xl-2 col-lg-2 col-md-2 col-sm-2">
            {
              this.state.stock == 0 ?
                <span className="stock-status out-stock">In stock</span>
                :
                <span className="stock-status">In stock</span>
            }
          </div>
          <div className="wishlist-table-cell dt-pd-addCart col-xl-2 col-lg-2 col-md-2 col-sm-2 a-t-c">
            <div className="table-interaction">
              {
                this.state.inCart ?
                  <a className="addBtndsb">Already in Cart</a>
                  :
                  <a className="addBtn" onClick={this._addCart}>Add To Cart</a>
              }
            </div>
          </div>

          <div className="mb-pd-price">
            {
              this.state.stock == 0 ?
                <span className="stock-status out-stock">In stock</span>
                :
                <span className="stock-status">In stock</span>
            }
            <span> HK ${this.state.price}</span>


          </div>
        </div>
        <div>
          <div className="mb-name-row">
            <div className="mb-pd-name">
              <Link to={`/product_detail/${this.props.pdid}`}>{this.state.name}</Link>
              <i onClick={this.removeItemInWishlist} className="fa fa-times mb-rm-btn-ico" aria-hidden="true" />
            </div>
            <a className="addBtn ml-auto" onClick={this._addCart}>Add To Cart</a>
          </div>
        </div>
        <hr className="row-divider" />
      </div>
    )
  }

  render() {
    return (
      this.renderProductRow()
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    wishlistContent: state.wishlistContent,
    currentLocale: state.localization,
    cartContent: state.cartContent,
  }
}

export default connect(mapStateToProps, actions)(Wishlist);