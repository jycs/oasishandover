import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import _ from "lodash";
import * as actions from "../../actions";
import WishlistItem from "./WishlistItem";
import { FormattedMessage } from "react-intl";
import "./Wishtlist.css";

class Wishlist extends Component {
  constructor() {
    super();
    this.state = {
      localWishlist: []
    };
  }

  componentDidMount() {
    // set timeout for fetch data too often.
    this.props.fetchProducts();
    this._syncWishlist()
    // this.props.updateCart("vas")
    console.log('====================================');
    console.log(this.props.wishlistContent.wishlistArr);
    console.log('====================================');
  }

  _syncWishlist() {
    this.setState({
      localWishlist: JSON.parse(localStorage.getItem("wishlistLocal")),
    }, () => {
      console.log("synced, the new wishlist " + this.state.colorSelected);
      console.log(this.state.localWishlist);
    })
  }

  _addCart = (id, name, color, img, qty, price) => {
    const itemDetails = {
      id: id,
      name,
      color: color,
      qty: qty,
      price: price,
      productImg: img,
      total: price * qty,
    }
    this.props.addCart(itemDetails).then(() => {
      localStorage.setItem("shoppingCartLocal", JSON.stringify(this.props.cartContent.cartArray));
      // this._removeWishList(id, color)
    }).then(() => {
      console.log("data added to cart")
    });


  }
  /** 
  _removeWishList = (wishlistItems, deletedItem) => {
    this.props.removeWishlist(wishlistItems).then(() => {
      console.log("removed persist redux")
      console.log(this.props.wishlistContent.wishlistArr)
      console.log(wishlistItems)
      this.setState({
        localWishlist: wishlistItems
      }, () => {
        localStorage.setItem("wishlistLocal", JSON.stringify(this.props.wishlistContent.wishlistArr));
        this._syncWishlist()
        // this.reRenderProducts()
      })
    })
  }
  */


  _removeWishList = (id, color, key) => {
    console.log("parent function")
    console.log(id, color, key)
    // 1. get cart data in state
    let localWishlist = this.state.localWishlist;
    // 2. find the index(location) of the specific product stored in the cart array
    let itemIndex = _.findIndex(localWishlist, function (element, index, collection) {
      return (element.id == id && element.color == color)
    })

    //3. replace content to the array with the latest qty
    console.log("found index", itemIndex)
    localWishlist.splice(itemIndex, 1);

    //4. set state with the updated array
    this.setState({
      localWishlist
    })

    //4. sync new array with the local storage
    this.props.removeWishlist(localWishlist).then(() => {
      localStorage.setItem("wishlistLocal", JSON.stringify(this.state.localWishlist));
      // this.fetchWishlistContent(this.props.match.params.id, this.state.colorSelected)
    });
    this.renderProducts()
  }


  // renderProducts() {
  //   this.reRenderProducts()
  // }
  renderConent() {
    return (
      <div className="rerenderes">
        {this.renderProducts()}
      </div>
    )
  }
  renderProducts() {
    if (_.isEmpty(this.state.localWishlist, true)) {
      return (
        <div className="empty-wl mr-auto ml-auto d-flex flex-column align-items-center justify-content-center">
          <img src="https://www.feelingsexy.com.au/images/newdesign/empty-wishlist.png" />
          <h5>Your wishlist is empty, add something to make it happy!</h5>
          <Link to="/product_list"><button className="">Go Shopping</button></Link>
        </div>
      )
    }

    return _.map(this.state.localWishlist, (t, i) => {
      return (

        <WishlistItem key={i} idInWishlist={i} pdid={t.id} img={t.img} pdcolor={t.color} handleAdd={this._addCart} handleRemove={this._removeWishList} wishlistData={this.state.localWishlist} />
      )
    });
  }


  reRenderProducts() {
    console.log("re rendered")
    if (_.isEmpty(this.state.localWishlist, true)) {
      return (
        <div className="empty-wl mr-auto ml-auto d-flex flex-column align-items-center justify-content-center">
          <img src="https://www.feelingsexy.com.au/images/newdesign/empty-wishlist.png" />
          <h5>Your wishlist is empty, add something to make it happy!</h5>
          <Link to="/product_list"><button className="">Go Shopping</button></Link>
        </div>
      )
    }
    /* map data returned, limited by 4  */
    return _.map(this.state.localWishlist, (t, i) => {
      return (

        <WishlistItem key={i} idInWishlist={i} pdid={t.id} img={t.img} pdcolor={t.color} handleAdd={this._addCart} handleRemove={this._removeWishList} wishlistData={this.state.localWishlist} />

      )
    });
  }


  render() {
    const renderProducts = this.renderProducts();
    return (
      <div className="wishlist-container">
        <div className="page-path">
          <ul>
            <li><Link to="/">Home</Link></li>
            <li>/</li>
            <li><Link to="/product_list">Shop</Link></li>
            <li>/</li>
            <li><Link to="/wishlist">Cart</Link></li>
          </ul>
        </div>
        <div className="wishlist-table-container">
          <div className="page-title"><h6>Wishlist</h6></div>
          <hr className="row-divider-title" />
          <div className="wishlist-table container">
            <div>
              <div className="row">
                <div className="wishlist-table-row-h d-flex flex-row">
                  <div className="rm-btn-title col-xl-1 col-lg-1 col-md-1 col-sm-1">
                    <h6>Remove</h6>
                  </div>
                  <div className="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <div><h6 className="pd-title">Products</h6></div>
                  </div>
                  <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                    <h6 className="unit-price">Unit Price</h6>
                  </div>
                  <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                    <h6>Status</h6>
                  </div>
                  <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                    <h6>Add To Cart</h6>
                  </div>
                </div>
                {renderProducts}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.fetchedProducts,
    wishlistContent: state.wishlistContent,
    currentLocale: state.localization,
    cartContent: state.cartContent,
  }
}

export default connect(mapStateToProps, actions)(Wishlist);