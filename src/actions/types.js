export const AUTH_USER = 'auth_user';
export const AUTH_ERROR = 'auth_error';
export const FETCH_PRODUCTS = "fetch_products";
export const FETCH_PRODUCT_DETAIL = "fetch_product_detail";
export const FETCH_CATALOG = "fetch_catalog";
export const ADD_CART = "add_cart";
export const UPDATE_CART = "update_cart";
export const REMOVE_ITEM = "remove_item";
export const UPDATE_LOCALE = "update_locale";
export const ADD_WISHLIST = "add_wishlist";
export const UPDATE_WISHLIST = "update_wishlist";
export const REMOVE_WISHLIST = "REMOVE_wishlist";
export const CLEAR_CART = "clear_cart";

