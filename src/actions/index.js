import {
  AUTH_USER,
  AUTH_ERROR,
  FETCH_PRODUCTS,
  FETCH_PRODUCT_DETAIL,
  RE_FETCH_PRODUCTS,
  FETCH_CATALOG,
  ADD_CART,
  UPDATE_CART,
  REMOVE_ITEM,
  CLEAR_CART,
  UPDATE_LOCALE,
  ADD_WISHLIST,
  REMOVE_WISHLIST
} from "./types";
import axios from "axios";
import qs from "qs";



export const signup = (formProps, cb) => async (dispatch) => { // cb, a callback from the form
  try {
    const response = await axios.post("https://www.oasisapi.herokuapp.com/signup", formProps);
    dispatch({
      type: AUTH_USER, payload: response.data.token
    });
    localStorage.setItem("token", response.data.token);
    cb();
  } catch (error) {
    dispatch({
      type: AUTH_ERROR, payload: "Email in use"
    });
  }
};

export const signin = (formProps, cb) => async (dispatch) => { // cb, a callback from 
  dispatch({
    type: AUTH_USER, payload: formProps
  });
  localStorage.setItem("token", formProps);
  cb();

};


export const signout = () => {
  localStorage.removeItem("token");
  return {
    type: AUTH_USER, payload: ""
  }
}

export const fetchProducts = (lang) => async (dispatch) => {
  try {

    const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)
    const res2 = await axios.get(`${gUrl}/api/product_output.php?lang=${lang}`);
    dispatch({
      type: FETCH_PRODUCTS, payload: res2
    });

  } catch (e) {
  }
}
export const fetchProductCatalog = (lang) => async (dispatch) => {
  try {

    const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)
    const res2 = await axios.get(`${gUrl}/api/load_category.php?lang=${lang}`);

    dispatch({
      type: FETCH_CATALOG, payload: res2
    });

  } catch (e) {
  }
}

export const addCart = (itemDetails) => async (dispatch) => {
  dispatch({
    type: ADD_CART, payload: itemDetails
  });
}

export const removedCart = (itemDetails) => async (dispatch) => {
  dispatch({
    type: REMOVE_ITEM, payload: itemDetails
  });
}
export const clearCart = (itemDetails) => async (dispatch) => {
  dispatch({
    type: CLEAR_CART, payload: itemDetails
  });
}


export const addWishlist = (id, color, img) => async (dispatch) => {
  const payload = {
    id,
    color,
    img
  }
  dispatch({
    type: ADD_WISHLIST, payload: payload
  })
}
export const removeWishlist = (payload) => async (dispatch) => {
  dispatch({
    type: REMOVE_WISHLIST, payload: payload
  })
}



export const updateCart = (itemDetails) => async (dispatch) => {
  dispatch({
    type: UPDATE_CART, payload: itemDetails
  });

}


export const reFetchProducts = () => async (dispatch) => {
  const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)
  const res2 = await axios.get(`${gUrl}/api/product_output.php`);
}
export const fetchProductDetail = (id, lang) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    const gUrl = (process.env.NODE_ENV == "production" ? process.env.REACT_APP_API_PROD_URL : process.env.REACT_APP_API_DEV_URL)
    const url = `${gUrl}/api/product_detail.php`

    const payload = {
      id: id,
      lang: lang
    }

    const res = await axios.post(url, qs.stringify(payload));
    dispatch({
      type: FETCH_PRODUCT_DETAIL, payload: res
    });

  } catch (e) {
  }
}

export const changeLanguage = (e) => async (dispatch) => {
  localStorage.setItem("localeLang", JSON.stringify(e));
  dispatch({
    type: UPDATE_LOCALE, payload: e
  });
}