import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
// import storage from 'redux-persist/lib/storage';
// import { persistReducer } from 'redux-persist';
import auth from "./auth"
import productsReducer from "./productsReducer";
import productDetailReducer from "./productDetailReducer";
import productCatalogReducer from "./catalogReducer";
import localization from "./localization";
import cartReducer from "./cart";
import wishlistReducer from "./wishlist";

const rootReducer = combineReducers({
  auth,
  form: formReducer,
  fetchedProducts: productsReducer,
  fetchedCatalog: productCatalogReducer,
  fetchedProductDetail: productDetailReducer,
  cartContent: cartReducer,
  localization: localization,
  wishlistContent: wishlistReducer
});

export default rootReducer;