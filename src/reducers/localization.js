import { UPDATE_LOCALE } from "../actions/types";


const INITIAL_STATE = {
  // currentLocale:""
}

export default (state = { INITIAL_STATE }, action) => {
  switch (action.type) {
    case UPDATE_LOCALE:
      return action.payload
    default:
      return state;
  }
};