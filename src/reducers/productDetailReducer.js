import { FETCH_PRODUCT_DETAIL } from "../actions/types";


const INITIAL_STATE = {
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_DETAIL:
      return action.payload.data
    default:
      return state;
  }
};