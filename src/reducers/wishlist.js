import { REMOVE_WISHLIST, ADD_WISHLIST } from "../actions/types";


const INITIAL_STATE = {
  wishlistArr: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_WISHLIST:
      return { ...state, wishlistArr: [...state.wishlistArr, action.payload] }

    case REMOVE_WISHLIST: {
      return { ...state, wishlistArr: action.payload }
    }
    default:
      return state;
  }
};