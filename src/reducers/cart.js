import { ADD_CART, UPDATE_CART, REMOVE_ITEM, CLEAR_CART } from "../actions/types";
// import _ from "lodash";

const INITIAL_STATE = {
  cartArray: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_CART:
      return { ...state, cartArray: [...state.cartArray, action.payload] }

    case UPDATE_CART: {
      // return action.payload
    }

    case REMOVE_ITEM: {
      return { ...state, cartArray: action.payload }
    }
    case CLEAR_CART: {
      return { ...state, cartArray: [] }
    }
    default:
      return state;
  }
};