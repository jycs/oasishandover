import { FETCH_CATALOG } from "../actions/types";


const INITIAL_STATE = {
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_CATALOG:
      return action.payload.data
    default:
      return state;
  }
};