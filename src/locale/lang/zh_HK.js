const zh_CN = {
  //Top Header
  createAccount: "登記新帳戶",
  myAccount: '我的帳戶',
  accountSettings: "帳戶設定",
  signIn: "會員登入",
  signOut: "登出",

  //Mid header
  myWishlist: "喜愛清單",
  myCart: "購物車",

  //Bot header
  home: "首頁",


  //home
  ourSellers: "品牌",
  topProducts: "熱銷產品",
  aboutSectiontitle: "創作屬於你的家",
  aboutSectionDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  aboutOasis: "關於 Oasis",
  featureProducts: "特色產品",
  fp_dining: "飯廳",
  fp_office: "辨公室",
  fp_studyroom: "書房",
  fp_living: "客廳",

  //product list
  products: "商品",
  categories: "分類篩選",
  priceFilter: "價錢",
  all: "所有",
  sortBy: "排序",
  defaultSorting: "預設排序",
  topRelated: "相關熱銷產品",
  productTags: "產品標籤",
  lpSorting: "最低價格",
  hpSorting: "最高價格",
  price: "價錢: ",
  filter: "篩選",
  showing: "正在顯示 ",
  results: " 結果",
  noProduct: "找不到相關產品",
  seeOthers: "查看其他產品",
  inStock: "有現貨",
  outStock: "缺貨",
  avalibility: "庫存狀態:",
  updateCart: "更新購物車",
  addCart: "加入購物車",
  overview: "商品簡介",
  specification: "產品尺寸",
  width: "闊度: ",
  depth: "深度: ",
  height: "高度: ",
  cate: "產品類別: ",
  share: "分享",
  alsoLike: "你可能會感興趣",
  shippingInfo: "運費",
  domesShip: "本地運送:",
  IntShip: "國際運送 :",
  shipWaive: "運費 ",
  shipWaiveInfo: "買滿HKD$1500 可括免運費",
  colorOptions: "顏色選擇",


  // product carts
  shoppingCartheader: "購物車",
  cartProducts: "產品名稱",
  unitPrice: "單價",
  qty: "數量",
  totalPrice: "總價",
  clearCart: "清空購物車",
  updateCartBtn: "更新購物車",
  proceed: "前往結帳",
  continueShopping: "繼續購物",
  couponCode: "優惠代碼",
  couponDesc: "輸入優惠代碼以獲取折扣.",
  applyCoupon: "使用優惠代碼",
  cartTotal: "產品總額",
  subTotal: "小計",
  grandTotal: "付款總額",

  creditSlips: "信用單",
  order: "訂單",
  personalInfo: "個人資料",
  shoppingGuide: "購物指南",
  storeDirectory: "商品目錄",
  allCat: "商品類別",
  productReview: "商品評價",
  aboutReturn: "關於退貨",
  trackDelivery: "訂單追蹤",
  aboutWebsite: "網站資訊",
  aboutUs: "關於我們",
  merchants: "商店加盟",
  contactUs: "聯絡我們",
  conditions: "使用條款",
  privacy: "私隱政策",
  address1: "香港九龍旺角弼街63號珍寶樓2樓",





}
export default zh_CN; 
