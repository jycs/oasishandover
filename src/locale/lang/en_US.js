const en_US = {
  //Top Header
  createAccount: "Create An Account",
  myAccount: 'My Account',
  accountSettings: "Account Settings",
  signIn: "Sign In",
  signOut: "Sign Out",

  //Mid header
  myWishlist: "My Wishlist",
  myCart: "My Cart",

  //Bot header
  home: "Home",

  //page indicator
  products: "Products",

  //home
  ourSellers: "Our Sellers",
  topProducts: "Top Products",
  aboutSectiontitle: "Create the home you've always wanted",
  aboutSectionDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  featureProducts: "Featured Products",
  aboutOasis: "About Oasis",
  fp_dining: "Dining",
  fp_office: "Office",
  fp_studyroom: "Studyroom",
  fp_living: "Living",


  //product list
  categories: "CATEGORIES",
  priceFilter: "FILTER BY PRICE",
  all: "All",
  defaultSorting: "Default Sorting",
  topRelated: "TOP RELATED PRODUCTS",
  productTags: "PRODUCT TAGS",
  lpSorting: "Lowest Price",
  hpSorting: "Highest Price",
  price: "Price: ",
  filter: "Filter",
  showing: "Showing",
  results: "results",
  noProduct: "No Product Found",
  seeOthers: "Checkout Others",

  //product detail
  inStock: "In stock",
  outStock: "Out of stock",
  avalibility: "Availability:",
  updateCart: "Update Cart",
  addCart: "Add Cart",
  overview: "Overview",
  specification: "Specification",
  width: "Width",
  depth: "Depth",
  height: "Height",
  cate: "Category: ",
  share: "Share",
  alsoLike: "You May Also Like",
  shippingInfo: "Shipping Information",
  domesShip: "Domestic Shipping:",
  IntShip: "International Shipping:",
  shipWaiveInfo: "Buying more than HK$ 1500,from will receive a free",
  shipWaive: "Shipping Fee",
  colorOptions: "Colors",

  // product carts
  shoppingCartheader: "Shopping Cart",
  cartProducts: "Products",
  unitPrice: "Unit Price",
  qty: "Quantity",
  totalPrice: "Total",
  clearCart: "Clear Shopping Cart",
  updateCartBtn: "Update Shopping Cart",
  proceed: "Proceed to Checkout",
  continueShopping: "Continue Shopping",
  couponCode: "Coupon Code",
  couponDesc: "Enter your coupon code if you have one.",
  applyCoupon: "Apply Coupon",
  cartTotal: "Cart Total",
  subTotal: "Subtotal",
  grandTotal: "Grandtotal",

  //wishlist


  order: "Orders",
  allCat: "All Category",
  creditSlips: "Credit Slips",
  personalInfo: "Personal Information",
  shoppingGuide: "Shopping Guide",
  storeDirectory: "Store Directory",
  productReview: "Product Reviews",
  aboutReturn: "Returns & Exchanges",
  trackDelivery: "Shipping & Delivery",
  aboutWebsite: "Website Info",
  aboutUs: "About Us ",
  merchants: "Merchants Recruitment",
  contactUs: "Contact Us ",
  conditions: "Terms & Condition",
  privacy: "Privacy Policy",
  address1: "2/F Chun Po House,",
  address2: "63 Bute St, Mong kok, Kowloon, ",
  address3: "Hong Kong",

}
export default en_US;