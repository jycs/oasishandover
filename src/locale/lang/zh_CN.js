const zh_CN = {
  //Top Header
  createAccount: "登记新帐户",
  myAccount: '我的帐户',
  accountSettings: "帐户设定",
  signIn: "会员登入",
  signOut: "登出",

  //Mid header
  myWishlist: "喜爱清单",
  myCart: "购物车",

  //Bot header
  home: "首页",


  //home
  ourSellers: "品牌",
  topProducts: "热销产品",
  aboutSectiontitle: "创作属于你的家",
  aboutSectionDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
  aboutOasis: "关于 Oasis",
  featureProducts: "特色产品",
  fp_dining: "饭厅",
  fp_office: "辨公室",
  fp_studyroom: "书房",
  fp_living: "客厅",

  //product list
  products: "商品",
  categories: "分类筛选",
  priceFilter: "价钱",
  all: "所有",
  sortBy: "排序",
  defaultSorting: "预设排序",
  topRelated: "相关热销产品",
  productTags: "产品标签",
  lpSorting: "最低价格",
  hpSorting: "最高价格",
  price: "价钱: ",
  filter: "筛选",
  showing: "正在显示 ",
  results: " 结果",
  noProduct: "找不到相关产品",
  seeOthers: "查看其他产品",
  inStock: "有现货",
  outStock: "缺货",
  avalibility: "库存状态:",
  updateCart: "更新购物车",
  addCart: "加入购物车",
  overview: "商品简介",
  specification: "产品尺寸",
  width: "阔度: ",
  depth: "深度: ",
  height: "高度: ",
  cate: "产品类别: ",
  share: "分享",
  alsoLike: "你可能会感兴趣",
  shippingInfo: "运费",
  domesShip: "本地运送:",
  IntShip: "国际运送 :",
  shipWaive: "运费 ",
  shipWaiveInfo: "买满HKD$1500 可括免运费",
  colorOptions: "颜色选择",


  // product carts
  shoppingCartheader: "购物车",
  cartProducts: "产品名称",
  unitPrice: "单价",
  qty: "数量",
  totalPrice: "总价",
  clearCart: "清空购物车",
  updateCartBtn: "更新购物车",
  proceed: "前往结帐",
  continueShopping: "继续购物",
  couponCode: "优惠代码",
  couponDesc: "输入优惠代码以获取折扣.",
  applyCoupon: "使用优惠代码",
  cartTotal: "产品总额",
  subTotal: "小计",
  grandTotal: "付款总额",

  creditSlips: "信用单",
  order: "订单",
  personalInfo: "个人资料",
  shoppingGuide: "购物指南",
  storeDirectory: "商品目录",
  allCat: "商品类别",
  productReview: "商品评价",
  aboutReturn: "关于退货",
  trackDelivery: "订单追踪",
  aboutWebsite: "网站资讯",
  aboutUs: "关于我们",
  merchants: "商店加盟",
  contactUs: "联络我们",
  conditions: "使用条款",
  privacy: "私隐政策",
  address1: "香港九龙旺角弼街63号珍宝楼2楼",
}
export default zh_CN; 