import React, { Component } from 'react';
import './App.css';
import NavigationBar from "../components/Navbar/NavigationBar";
import Footer from "../components/Footer/Footer";

import { addLocaleData, IntlProvider } from 'react-intl';
import zh from 'react-intl/locale-data/zh';
import en from 'react-intl/locale-data/en';
import zh_CN from '../locale/lang/zh_CN';
import zh_HK from '../locale/lang/zh_HK';
import en_US from '../locale/lang/en_US';
addLocaleData([...en, ...zh]);


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currLocale: JSON.parse(localStorage.getItem("localeLang")).locale ? JSON.parse(localStorage.getItem("localeLang")).locale : "zh",
      currLang: JSON.parse(localStorage.getItem("localeLang")).lang ? JSON.parse(localStorage.getItem("localeLang")).lang : "tc",
    }
  }
  componentDidMount() {
  }
  render() {
    let messages = {}
    messages['en'] = en_US;
    messages['sc'] = zh_CN;
    messages['tc'] = zh_HK;



    return (
      <IntlProvider
        locale={this.state.currLocale}
        messages={
          messages[this.state.currLang]
        }
      >
        <div>
          <NavigationBar currLocale={this.state.currLocale} currLang={this.state.currLang} />
          {this.props.children}
          <Footer />

        </div>
      </IntlProvider>
    );
  }
}

export default App;
