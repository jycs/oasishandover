import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react'
import thunk from "redux-thunk";
import registerServiceWorker from './registerServiceWorker';
import App from './container/App';
import Home from './components/Home/Home';
import Feature from "./components/Feature";
import Signup from './components/auth/Signup';
import Signin from './components/auth/Signin';
import ProductList from './components/ProductList/ProductList';
import Signout from './components/auth/Signout';
import rootReducer from "./reducers"
import ScrollToTop from "./container/ScrollToTop";
import './index.css';
import ProductDetail from './components/ProductDetail/ProductDetail';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';
import Wishlist from './components/Wishlist/Wishlist';
import Checkout from './components/Checkout/Checkout';
import MyAccount from './components/MyAccount/MyAccount';
import cms from './components/Others/cms';



// https://github.com/rt2zz/redux-persist/issues/58
// redux persist tutorial


let langPreference = localStorage.getItem("localeLang")
langPreference ? langPreference : localStorage.setItem("localeLang", JSON.stringify({ locale: "zh", lang: "tc", title: "中文(繁)" }));

const persistConfig = {
  key: 'cart',
  whitelist: ["cartContent", "wishlistContent", "auth"],
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  persistedReducer,
  {
    auth: { authenticated: localStorage.getItem("token") },
    localization: JSON.parse(localStorage.getItem("localeLang"))
  },
  applyMiddleware(thunk)
)

let persistor = persistStore(store);
ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <ScrollToTop>

          <App>
            <Route path="/" exact component={Home} />
            <Route path="/signup" component={Signup} />
            {/*<Route path="/feature" component={Feature} />*/}
            <Route path="/product_list/" component={ProductList} />
            <Route path="/signout" component={Signout} />
            <Route path="/signin" component={Signin} />
            <Route path="/product_detail/:id" component={ProductDetail} />
            <Route path="/Wishlist" component={Wishlist} />
            <Route path="/cart" component={ShoppingCart} />
            <Route path="/checkout" component={Checkout} />
            <Route path="/MyAccount" component={MyAccount} />
            <Route path="/thecmss" component={cms} />
          </App>

        </ScrollToTop>
      </BrowserRouter>
    </PersistGate>
  </Provider>
  , document.getElementById('root'));
registerServiceWorker();
